﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by coded UI test builder.
//      Version: 14.0.0.0
//
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace UITestProject
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public partial class UIMap
    {
        
        /// <summary>
        /// RecordedMethod1 - Use 'RecordedMethod1Params' to pass parameters into this method.
        /// </summary>
        public void RecordedMethod1()
        {
            #region Variable Declarations
            WinEdit uIDistancetextBoxEdit = this.UIForm1Window.UIDistancetextBoxWindow.UIDistancetextBoxEdit;
            WinButton uICalculateTimeButton = this.UIForm1Window.UICalculateTimeWindow.UICalculateTimeButton;
            #endregion

            // Type '2140000000000' in 'DistancetextBox' text box
            uIDistancetextBoxEdit.Text = this.RecordedMethod1Params.UIDistancetextBoxEditText;

            // Click 'Calculate Time' button
            Mouse.Click(uICalculateTimeButton, new Point(54, 13));
        }
        
        /// <summary>
        /// RecordedMethod2 - Use 'RecordedMethod2Params' to pass parameters into this method.
        /// </summary>
        public void RecordedMethod2()
        {
            #region Variable Declarations
            WinEdit uIDistancetextBoxEdit = this.UIForm1Window.UIDistancetextBoxWindow.UIDistancetextBoxEdit;
            WinEdit uITextBox1Edit = this.UIForm1Window.UITextBox1Window.UITextBox1Edit;
            WinWindow uITextBox1Window = this.UIForm1Window.UIOutputsGroup.UITextBox1Window;
            WinButton uICalculateTimeButton = this.UIForm1Window.UICalculateTimeWindow.UICalculateTimeButton;
            #endregion

            // Type '2333333333333' in 'DistancetextBox' text box
            uIDistancetextBoxEdit.Text = this.RecordedMethod2Params.UIDistancetextBoxEditText;

            // Click 'textBox1' text box
            Mouse.Click(uITextBox1Edit, new Point(60, 3));

            // Move using Right button 'textBox1' text box to 'textBox1' window
            uITextBox1Window.EnsureClickable(new Point(33, 18));
            Mouse.StartDragging(uITextBox1Edit, new Point(38, 13), MouseButtons.Right, ModifierKeys.None);
            Mouse.StopDragging(uITextBox1Window, new Point(33, 18));

            // Type '2140000000000' in 'DistancetextBox' text box
            uIDistancetextBoxEdit.Text = this.RecordedMethod2Params.UIDistancetextBoxEditText1;

            // Click 'Calculate Time' button
            Mouse.Click(uICalculateTimeButton, new Point(82, 16));

            // Click 'textBox1' text box
            Mouse.Click(uITextBox1Edit, new Point(42, 7));

            // Right-Click 'textBox1' text box
            Mouse.Click(uITextBox1Edit, MouseButtons.Right, ModifierKeys.None, new Point(34, 6));
        }
        
        /// <summary>
        /// RecordedMethod3 - Use 'RecordedMethod3Params' to pass parameters into this method.
        /// </summary>
        public void RecordedMethod3()
        {
            #region Variable Declarations
            WinEdit uIDistancetextBoxEdit = this.UIForm1Window.UIDistancetextBoxWindow.UIDistancetextBoxEdit;
            WinButton uICalculateTimeButton = this.UIForm1Window.UICalculateTimeWindow.UICalculateTimeButton;
            #endregion

            // Type '2140000000000' in 'DistancetextBox' text box
            uIDistancetextBoxEdit.Text = this.RecordedMethod3Params.UIDistancetextBoxEditText;

            // Click 'Calculate Time' button
            Mouse.Click(uICalculateTimeButton, new Point(78, 10));
        }
        
        /// <summary>
        /// RecordedMethod4 - Use 'RecordedMethod4Params' to pass parameters into this method.
        /// </summary>
        public void RecordedMethod4()
        {
            #region Variable Declarations
            WinEdit uIDistancetextBoxEdit = this.UIForm1Window.UIDistancetextBoxWindow.UIDistancetextBoxEdit;
            WinButton uICalculateTimeButton = this.UIForm1Window.UICalculateTimeWindow.UICalculateTimeButton;
            WinWindow uITextBox1Window = this.UIForm1Window.UIOutputsGroup.UITextBox1Window;
            WinEdit uITextBox1Edit = this.UIForm1Window.UITextBox1Window.UITextBox1Edit;
            WinButton uICloseButton = this.UIForm1Window.UIForm1TitleBar.UICloseButton;
            #endregion

            // Type '2140000000000' in 'DistancetextBox' text box
            uIDistancetextBoxEdit.Text = this.RecordedMethod4Params.UIDistancetextBoxEditText;

            // Click 'Calculate Time' button
            Mouse.Click(uICalculateTimeButton, new Point(78, 10));

            // Type '2140000000000' in 'DistancetextBox' text box
            uIDistancetextBoxEdit.Text = this.RecordedMethod4Params.UIDistancetextBoxEditText1;

            // Click 'Calculate Time' button
            Mouse.Click(uICalculateTimeButton, new Point(43, 16));

            // Type 'Alt + u' in 'Calculate Time' button
            Keyboard.SendKeys(uICalculateTimeButton, this.RecordedMethod4Params.UICalculateTimeButtonSendKeys, ModifierKeys.Alt);

            // Click 'textBox1' window
            Mouse.Click(uITextBox1Window, new Point(26, 1));

            // Click 'textBox1' text box
            Mouse.Click(uITextBox1Edit, new Point(20, 10));

            // Type 'Alt + u' in 'textBox1' text box
            Keyboard.SendKeys(uITextBox1Edit, this.RecordedMethod4Params.UITextBox1EditSendKeys, ModifierKeys.Alt);

            // Type 'Alt, Shift + u' in 'textBox1' text box
            Keyboard.SendKeys(uITextBox1Edit, this.RecordedMethod4Params.UITextBox1EditSendKeys1, (ModifierKeys.Alt | ModifierKeys.Shift));

            // Click 'Close' button
            Mouse.Click(uICloseButton, new Point(35, 15));
        }
        
        #region Properties
        public virtual RecordedMethod1Params RecordedMethod1Params
        {
            get
            {
                if ((this.mRecordedMethod1Params == null))
                {
                    this.mRecordedMethod1Params = new RecordedMethod1Params();
                }
                return this.mRecordedMethod1Params;
            }
        }
        
        public virtual RecordedMethod2Params RecordedMethod2Params
        {
            get
            {
                if ((this.mRecordedMethod2Params == null))
                {
                    this.mRecordedMethod2Params = new RecordedMethod2Params();
                }
                return this.mRecordedMethod2Params;
            }
        }
        
        public virtual RecordedMethod3Params RecordedMethod3Params
        {
            get
            {
                if ((this.mRecordedMethod3Params == null))
                {
                    this.mRecordedMethod3Params = new RecordedMethod3Params();
                }
                return this.mRecordedMethod3Params;
            }
        }
        
        public virtual RecordedMethod4Params RecordedMethod4Params
        {
            get
            {
                if ((this.mRecordedMethod4Params == null))
                {
                    this.mRecordedMethod4Params = new RecordedMethod4Params();
                }
                return this.mRecordedMethod4Params;
            }
        }
        
        public UIForm1Window UIForm1Window
        {
            get
            {
                if ((this.mUIForm1Window == null))
                {
                    this.mUIForm1Window = new UIForm1Window();
                }
                return this.mUIForm1Window;
            }
        }
        #endregion
        
        #region Fields
        private RecordedMethod1Params mRecordedMethod1Params;
        
        private RecordedMethod2Params mRecordedMethod2Params;
        
        private RecordedMethod3Params mRecordedMethod3Params;
        
        private RecordedMethod4Params mRecordedMethod4Params;
        
        private UIForm1Window mUIForm1Window;
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'RecordedMethod1'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class RecordedMethod1Params
    {
        
        #region Fields
        /// <summary>
        /// Type '2140000000000' in 'DistancetextBox' text box
        /// </summary>
        public string UIDistancetextBoxEditText = "2140000000000";
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'RecordedMethod2'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class RecordedMethod2Params
    {
        
        #region Fields
        /// <summary>
        /// Type '2333333333333' in 'DistancetextBox' text box
        /// </summary>
        public string UIDistancetextBoxEditText = "2333333333333";
        
        /// <summary>
        /// Type '2140000000000' in 'DistancetextBox' text box
        /// </summary>
        public string UIDistancetextBoxEditText1 = "2140000000000";
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'RecordedMethod3'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class RecordedMethod3Params
    {
        
        #region Fields
        /// <summary>
        /// Type '2140000000000' in 'DistancetextBox' text box
        /// </summary>
        public string UIDistancetextBoxEditText = "2140000000000";
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'RecordedMethod4'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class RecordedMethod4Params
    {
        
        #region Fields
        /// <summary>
        /// Type '2140000000000' in 'DistancetextBox' text box
        /// </summary>
        public string UIDistancetextBoxEditText = "2140000000000";
        
        /// <summary>
        /// Type '2140000000000' in 'DistancetextBox' text box
        /// </summary>
        public string UIDistancetextBoxEditText1 = "2140000000000";
        
        /// <summary>
        /// Type 'Alt + u' in 'Calculate Time' button
        /// </summary>
        public string UICalculateTimeButtonSendKeys = "u";
        
        /// <summary>
        /// Type 'Alt + u' in 'textBox1' text box
        /// </summary>
        public string UITextBox1EditSendKeys = "u";
        
        /// <summary>
        /// Type 'Alt, Shift + u' in 'textBox1' text box
        /// </summary>
        public string UITextBox1EditSendKeys1 = "u";
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class UIForm1Window : WinWindow
    {
        
        public UIForm1Window()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.Name] = "Form1";
            this.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.Window", PropertyExpressionOperator.Contains));
            this.WindowTitles.Add("Form1");
            #endregion
        }
        
        #region Properties
        public UIDistancetextBoxWindow UIDistancetextBoxWindow
        {
            get
            {
                if ((this.mUIDistancetextBoxWindow == null))
                {
                    this.mUIDistancetextBoxWindow = new UIDistancetextBoxWindow(this);
                }
                return this.mUIDistancetextBoxWindow;
            }
        }
        
        public UITextBox1Window UITextBox1Window
        {
            get
            {
                if ((this.mUITextBox1Window == null))
                {
                    this.mUITextBox1Window = new UITextBox1Window(this);
                }
                return this.mUITextBox1Window;
            }
        }
        
        public UIOutputsGroup UIOutputsGroup
        {
            get
            {
                if ((this.mUIOutputsGroup == null))
                {
                    this.mUIOutputsGroup = new UIOutputsGroup(this);
                }
                return this.mUIOutputsGroup;
            }
        }
        
        public UICalculateTimeWindow UICalculateTimeWindow
        {
            get
            {
                if ((this.mUICalculateTimeWindow == null))
                {
                    this.mUICalculateTimeWindow = new UICalculateTimeWindow(this);
                }
                return this.mUICalculateTimeWindow;
            }
        }
        
        public UIForm1TitleBar UIForm1TitleBar
        {
            get
            {
                if ((this.mUIForm1TitleBar == null))
                {
                    this.mUIForm1TitleBar = new UIForm1TitleBar(this);
                }
                return this.mUIForm1TitleBar;
            }
        }
        #endregion
        
        #region Fields
        private UIDistancetextBoxWindow mUIDistancetextBoxWindow;
        
        private UITextBox1Window mUITextBox1Window;
        
        private UIOutputsGroup mUIOutputsGroup;
        
        private UICalculateTimeWindow mUICalculateTimeWindow;
        
        private UIForm1TitleBar mUIForm1TitleBar;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class UIDistancetextBoxWindow : WinWindow
    {
        
        public UIDistancetextBoxWindow(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.ControlName] = "DistancetextBox";
            this.WindowTitles.Add("Form1");
            #endregion
        }
        
        #region Properties
        public WinEdit UIDistancetextBoxEdit
        {
            get
            {
                if ((this.mUIDistancetextBoxEdit == null))
                {
                    this.mUIDistancetextBoxEdit = new WinEdit(this);
                    #region Search Criteria
                    this.mUIDistancetextBoxEdit.WindowTitles.Add("Form1");
                    #endregion
                }
                return this.mUIDistancetextBoxEdit;
            }
        }
        #endregion
        
        #region Fields
        private WinEdit mUIDistancetextBoxEdit;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class UITextBox1Window : WinWindow
    {
        
        public UITextBox1Window(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.ControlName] = "textBox1";
            this.WindowTitles.Add("Form1");
            #endregion
        }
        
        #region Properties
        public WinEdit UITextBox1Edit
        {
            get
            {
                if ((this.mUITextBox1Edit == null))
                {
                    this.mUITextBox1Edit = new WinEdit(this);
                    #region Search Criteria
                    this.mUITextBox1Edit.SearchProperties[WinEdit.PropertyNames.Name] = "Water";
                    this.mUITextBox1Edit.WindowTitles.Add("Form1");
                    #endregion
                }
                return this.mUITextBox1Edit;
            }
        }
        #endregion
        
        #region Fields
        private WinEdit mUITextBox1Edit;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class UIOutputsGroup : WinGroup
    {
        
        public UIOutputsGroup(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinControl.PropertyNames.Name] = "Outputs";
            this.WindowTitles.Add("Form1");
            #endregion
        }
        
        #region Properties
        public WinWindow UITextBox1Window
        {
            get
            {
                if ((this.mUITextBox1Window == null))
                {
                    this.mUITextBox1Window = new WinWindow(this);
                    #region Search Criteria
                    this.mUITextBox1Window.SearchProperties[WinWindow.PropertyNames.AccessibleName] = "Water";
                    this.mUITextBox1Window.SearchProperties.Add(new PropertyExpression(WinWindow.PropertyNames.ClassName, "WindowsForms10.EDIT", PropertyExpressionOperator.Contains));
                    this.mUITextBox1Window.WindowTitles.Add("Form1");
                    #endregion
                }
                return this.mUITextBox1Window;
            }
        }
        #endregion
        
        #region Fields
        private WinWindow mUITextBox1Window;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class UICalculateTimeWindow : WinWindow
    {
        
        public UICalculateTimeWindow(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.ControlName] = "Calcbutton";
            this.WindowTitles.Add("Form1");
            #endregion
        }
        
        #region Properties
        public WinButton UICalculateTimeButton
        {
            get
            {
                if ((this.mUICalculateTimeButton == null))
                {
                    this.mUICalculateTimeButton = new WinButton(this);
                    #region Search Criteria
                    this.mUICalculateTimeButton.SearchProperties[WinButton.PropertyNames.Name] = "Calculate Time";
                    this.mUICalculateTimeButton.WindowTitles.Add("Form1");
                    #endregion
                }
                return this.mUICalculateTimeButton;
            }
        }
        #endregion
        
        #region Fields
        private WinButton mUICalculateTimeButton;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class UIForm1TitleBar : WinTitleBar
    {
        
        public UIForm1TitleBar(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.WindowTitles.Add("Form1");
            #endregion
        }
        
        #region Properties
        public WinButton UICloseButton
        {
            get
            {
                if ((this.mUICloseButton == null))
                {
                    this.mUICloseButton = new WinButton(this);
                    #region Search Criteria
                    this.mUICloseButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
                    this.mUICloseButton.WindowTitles.Add("Form1");
                    #endregion
                }
                return this.mUICloseButton;
            }
        }
        #endregion
        
        #region Fields
        private WinButton mUICloseButton;
        #endregion
    }
}
