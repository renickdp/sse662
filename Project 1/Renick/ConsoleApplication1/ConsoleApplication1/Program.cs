﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        class Coordinate
        {
            public int x;
            public int y;

            public Coordinate(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            public Coordinate(int x)
            {
                this.x = x;
                this.y = 3;
            }
        };

        static void Main(string[] args)
        {
            ArrayList untyped = new ArrayList();
            untyped.Add(1);
            untyped.Add("Hello, World!");
            untyped.Add(true);

            foreach (object item in untyped)
            {
                Console.WriteLine(item);
                Console.WriteLine(item.GetType());
            }
            Console.ReadLine();

            List<Coordinate> cords = new List<Coordinate>();
            Coordinate coord1 = new Coordinate(5,6);

            Coordinate coord2 = new Coordinate(6, 7);
            cords.Add(coord2);
            cords.Add(coord1);

            foreach (Coordinate item in cords)
            {
                Console.WriteLine(item);
                var q =item.x;
                
            }
            Console.ReadLine();
        }
    }
}
