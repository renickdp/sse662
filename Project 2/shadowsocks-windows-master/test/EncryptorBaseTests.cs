﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shadowsocks.Controller;
using Shadowsocks.Encryption.AEAD;
using Shadowsocks.Encryption.Stream;

namespace Sse662Tests
{
    [TestClass]
    public class EncryptorBaseTests
    {
        [TestMethod]
        public void EncryptorBase_AEADEncryptorTest()
        {
            var encryptor = new AEADMbedTLSEncryptor("aes-128-gcm", "password");

            byte[] input = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            byte[] encrypted = new byte[1024];
            byte[] output = new byte[1024];
            int encryptedLength = 0;
            int outputLength = 0;

            encryptor.EncryptUDP(input, 18, encrypted, out encryptedLength);
            encryptor.DecryptUDP(encrypted, encryptedLength, output, out outputLength);

            Assert.AreEqual(18, outputLength);
            for (int i = 0; i < input.Length; i++)
            {
                Assert.AreEqual(input[i], output[i]);
            }
        }

        [TestMethod]
        public void EncryptorBase_StreamEncryptor()
        {
            var encryptor = new StreamMbedTLSEncryptor("aes-128-cfb", "password");

            byte[] input = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            byte[] encrypted = new byte[1024];
            byte[] output = new byte[1024];
            int encryptedLength = 0;
            int outputLength = 0;

            encryptor.EncryptUDP(input, 18, encrypted, out encryptedLength);
            encryptor.DecryptUDP(encrypted, encryptedLength, output, out outputLength);

            Assert.AreEqual(18, outputLength);
            for (int i = 0; i < input.Length; i++)
            {
                Assert.AreEqual(input[i], output[i]);
            }
        }
    }
}
