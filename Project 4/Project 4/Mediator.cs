﻿using System.Windows.Forms;

namespace Project_4
{
    public class TotalMediator
    {
        public TextBox subtotalBox;
        public TextBox taxBox;
        public TextBox totalBox;

        public TotalMediator(TextBox subtotalBox, TextBox taxBox, TextBox totalBox)
        {
            this.subtotalBox = subtotalBox;
            this.taxBox = taxBox;
            this.totalBox = totalBox;
        }

        public void incrementSubtotal(decimal addedQty)
        {
            decimal subtotal = 0;

            if (this.subtotalBox.Text != "")
            {
                subtotal = decimal.Parse(this.subtotalBox.Text);
            }
            subtotal += addedQty;
            decimal totalTax = subtotal * .07m;

            this.subtotalBox.Text = subtotal.ToString();
            this.taxBox.Text = totalTax.ToString();
            this.totalBox.Text = (subtotal + totalTax).ToString();
        }

        public void clear()
        {
            this.subtotalBox.Text = "";
            this.taxBox.Text = "";
            this.totalBox.Text = "";
        }
    }
}