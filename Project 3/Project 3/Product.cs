﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    public class Product
    {
        public string productName;
        public decimal ProductPrice;

        private string _displayValue;

        public Product(string Name, decimal Price)
        {
            productName = Name;
            ProductPrice = Price;
        }
        
        public override string ToString()
        {
           return this.productName + "  $" + Convert.ToString(this.ProductPrice);
        }

        public bool Equals(object obj)
        {
            if (!(obj is Product))
            {
                return false;
            }
            Product other = (Product)obj;
            return (this.productName == other.productName &&
                    this.ProductPrice == other.ProductPrice); 

        }

        public override int GetHashCode()
        {
            return this.productName.GetHashCode() ^ this.ProductPrice.GetHashCode();
        }
    }
}
