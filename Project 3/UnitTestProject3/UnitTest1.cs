﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Project_3;
using System.Collections.Generic;
using System.Windows.Forms;

namespace UnitTestProject3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void  Zerototaltest()
        {
            Order order = new Order();

            Assert.AreEqual(0.0m, order.calculateTotal());
        }

        [TestMethod]
        public void CalculateTotal()
        {
            Order order = new Order();

            order.addItem(new Product("Burger", 5.99m));

            Assert.AreEqual(5.99m, order.calculateTotal());
        }

        [TestMethod]
        public void CalculateSubTotal()
        {
            Order order = new Order();

            order.addItem(new Product("Burger", 6.00m));
            order.addItem(new Product("Burrito", 4.00m));

            Assert.AreEqual(10.00m, order.SubtotalCalculator.Calculate(order));
        }


        //command pattern tests    
        [TestMethod]
        public void CoupontestApplyPercentage()
        {
            Order order = new Order();
            order.addItem(new Product("Taco", 2.00m));
            order.addItem(new Product("Taco", 2.00m));
            PercentageOffCoupon coupon = new PercentageOffCoupon(15, order);
            coupon.execute();
            Assert.AreEqual(3.4m, order.calculateTotal());
        }

        [TestMethod]
        public void CoupontestFreeItem()
        {
            Order order = new Order();
            order.addItem(new Product("Taco", 2.00m));
            order.addItem(new Product("Taco", 2.00m));
            ItemCoupon coupon = new ItemCoupon("Drink", order);
            coupon.execute();
            Product product = new Product("Drink", 0.0m);
            Assert.AreEqual(4.0m, order.calculateTotal());
            Assert.AreEqual(product, order.nameslist[2]);
        }

        [TestMethod]
        public void CoupontestUndoPercentage()
        {
            Order order = new Order();
            order.addItem(new Product("Taco", 1.70m));
            order.addItem(new Product("Taco", 1.70m));
            PercentageOffCoupon coupon = new PercentageOffCoupon(15, order);    
            coupon.undo();
            Assert.AreEqual(4.0m, order.calculateTotal());

        }

        [TestMethod]
        public void CoupontestUndoFreeItem()
        {
            Order order = new Order();
            order.addItem(new Product("Taco", 2.00m));
            order.addItem(new Product("Taco", 0.00m));
            ItemCoupon coupon = new ItemCoupon("Taco", order);
            coupon.undo();

            Product product = new Product("Taco", 0.0m);
            Assert.IsFalse(order.nameslist.Contains(product));
            Assert.AreEqual(2.0m, order.calculateTotal());
        }


        //observer pattern tests

        [TestMethod]
        public void ObserverFoodStoreTests()
        {
            ReceiptObservable observable = new ReceiptObservable();
            ReceiptObserver observer = new ReceiptObserver();

            observable.register(observer);

            WindowFactory windowFactory = new FoodWindowFactory();

            List<string> lines = new List<string>();
            lines.Add("Hamburger");
            FoodReceipt receipt = new FoodReceipt(windowFactory, lines);
            ReceiptEvent evt = new ReceiptEvent(receipt);
            observable.notify(evt);

            Assert.AreEqual(receipt.lines[1], "Thanks for visiting patterns food chain");
        }

        [TestMethod]
        public void ObserverCandyStoreTests()
        {
            ReceiptObservable observable = new ReceiptObservable();
            CandyReceiptObserver observer = new CandyReceiptObserver();

            observable.register(observer);

            WindowFactory windowFactory = new CandyWindowFactory();

            List<string> lines = new List<string>();
            lines.Add("Lollipop");
            CandyReceipt receipt = new CandyReceipt(windowFactory, lines);
            ReceiptEvent evt = new ReceiptEvent(receipt);
            observable.notify(evt);

            Assert.AreEqual(receipt.lines[1], "Thanks for shopping at the Candy Store!");
        }

        [TestMethod]
        public void AbstractFactoryFoodStoreForm()
        {
            FoodWindowFactory foodwindow = new FoodWindowFactory();
            Form foodstore = foodwindow.CreateStoreForm();
            Assert.IsTrue(foodstore is Form1);
        }

        [TestMethod]
        public void AbstractFactoryFoodReceiptForm()
        {
            FoodWindowFactory foodwindow = new FoodWindowFactory();
            List<string> lines = new List<string>();
            Form foodstore = foodwindow.CreateReceiptForm(lines);
            Assert.IsTrue(foodstore is FoodReceipt);
        }

        [TestMethod]
        public void AbstractFactoryCandyStoreForm()
        {
            CandyWindowFactory candywindow = new CandyWindowFactory();
            Form candystore = candywindow.CreateStoreForm();
            Assert.IsInstanceOfType(candystore, typeof(Form2));
        }

        [TestMethod]
        public void AbstractFactoryCandyReceiptForm()
        {
            CandyWindowFactory candywindow = new CandyWindowFactory();
            List<string> lines = new List<string>();
            Form candyform = candywindow.CreateReceiptForm(lines);
            Assert.IsTrue(candyform is CandyReceipt);
        }

    }
}
