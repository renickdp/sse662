﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{
    public abstract class AbstractReceipt: Form
    {

        private List<string> _lines;
        public List<string> lines
        {
            get { return this._lines; }
            set
            {
                this._lines = value;
                this.updateTextBox();
            }
        }
        abstract public void updateTextBox();
    };

    public partial class FoodReceipt : AbstractReceipt
    {
        public WindowFactory windowFactory;

        public FoodReceipt(WindowFactory windowFactory, List<string> lines)
        {
            InitializeComponent();
            this.windowFactory = windowFactory;
            this.lines = lines;
            this.updateTextBox();
        }

        public override void updateTextBox()
        {
            richTextBox1.Text = string.Join("\n", this.lines);
            richTextBox1.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
