﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    public class Order
    {
        public List<Product> nameslist;
        public ICalculator SubtotalCalculator;
        public ICalculator TaxCalculator;
        public Coupon appliedCoupon;


        public Order()
        {
            this.nameslist = new List<Product>();
            this.SubtotalCalculator = new USDSubtotalCalculator();
            this.TaxCalculator = new TaxCalculator();
            this.appliedCoupon = null;
        }
  
        public void addItem(Product productToAdd)
        {
            nameslist.Add(productToAdd);
        }

        public decimal calculateTotal()
        {
            return this.SubtotalCalculator.Calculate(this);
        }

        public List<string> receiptLines
        {
            get
            {
                List<string> lines = new List<string>();
                foreach (Product item in this.nameslist)
                {
                    lines.Add(item.ToString());
                }
                return lines;
            }
        }
   }
}