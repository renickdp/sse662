﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shadowsocks.Model;

namespace test
{
    [TestClass]
    public class GetDefaultServerTests
    {
        [TestMethod]
        public void GetDefaultServerTest1()
        {
            var config = Configuration.Load();
            var server = config.GetCurrentServer();

            Assert.AreEqual("", server.server);
            Assert.AreEqual(8388, server.server_port);
            Assert.AreEqual("aes-256-cfb", server.method);
            Assert.AreEqual("", server.plugin);
            Assert.AreEqual("", server.plugin_opts);
            Assert.AreEqual("", server.password);
            Assert.AreEqual("", server.remarks);
            Assert.AreEqual(5, server.timeout);
        }
    }
}
