﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{
    public partial class CandyReceipt : AbstractReceipt
    {

        public WindowFactory windowFactory;
        public CandyReceipt(WindowFactory windowFactory, List<string> lines)
        {
            InitializeComponent();
            this.lines = lines;
            this.windowFactory = windowFactory;
            this.updateTextBox();
        }

        public override void updateTextBox()
        {
            richTextBox1.Text = string.Join("\n", this.lines);
            richTextBox1.Refresh();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
