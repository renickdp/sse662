﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using ScreenToGif;
using ScreenToGif.Encoding;

namespace GifRecorderTests
{
    [TestClass]
    public class LZWEncoderTests
    {
        private const string IMG_FOLDER = @"..\..\images";

        private List<Bitmap> GetTestFrames()
        {
            List<Bitmap> list = new List<Bitmap>();

            list.Add(new Bitmap(Path.Combine(IMG_FOLDER, "blue_small.png")));
            list.Add(new Bitmap(Path.Combine(IMG_FOLDER, "green_small.png")));

            return list;
        }

        [TestMethod]
        public void LZWEncoderTest_OutputTest()
        {
            var file = Path.Combine(IMG_FOLDER, "LZW_test.gif");
            var frames = GetTestFrames();

            using (var encoder = new AnimatedGifEncoder())
            {
                encoder.Start(file);
                encoder.SetQuality(10);
                encoder.SetRepeat(0);

                encoder.SetTransparent(System.Drawing.Color.LimeGreen);
                encoder.SetDispose(1);

                foreach (Bitmap frame in frames)
                {
                    encoder.SetDelay(100);
                    encoder.AddFrame(frame, 0, 0);
                }
            }

            Thread.Sleep(100);

            using (var fStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                byte[] buf = new byte[2048];
                long len = fStream.Length;
                int read = fStream.Read(buf, 0, 2048);

                Assert.AreEqual(71, buf[0]);
                Assert.AreEqual(247, buf[10]);
                Assert.AreEqual(4, buf[100]);
                Assert.AreEqual(19, buf[1000]);
                Assert.AreEqual(250, buf[1600]);
            }
        }
    }
}
