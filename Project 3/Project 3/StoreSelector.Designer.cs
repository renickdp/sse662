﻿namespace Project_3
{
    partial class StoreSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FoodStorebtn = new System.Windows.Forms.Button();
            this.CandyStorebtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // FoodStorebtn
            // 
            this.FoodStorebtn.Location = new System.Drawing.Point(75, 152);
            this.FoodStorebtn.Name = "FoodStorebtn";
            this.FoodStorebtn.Size = new System.Drawing.Size(75, 23);
            this.FoodStorebtn.TabIndex = 0;
            this.FoodStorebtn.Text = "Food Store";
            this.FoodStorebtn.UseVisualStyleBackColor = true;
            this.FoodStorebtn.Click += new System.EventHandler(this.FoodStorebtn_Click);
            // 
            // CandyStorebtn
            // 
            this.CandyStorebtn.Location = new System.Drawing.Point(277, 152);
            this.CandyStorebtn.Name = "CandyStorebtn";
            this.CandyStorebtn.Size = new System.Drawing.Size(75, 23);
            this.CandyStorebtn.TabIndex = 1;
            this.CandyStorebtn.Text = "Candy Store";
            this.CandyStorebtn.UseVisualStyleBackColor = true;
            this.CandyStorebtn.Click += new System.EventHandler(this.CandyStorebtn_Click);
            // 
            // StoreSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 261);
            this.Controls.Add(this.CandyStorebtn);
            this.Controls.Add(this.FoodStorebtn);
            this.Name = "StoreSelector";
            this.Text = "StoreSelector";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button FoodStorebtn;
        private System.Windows.Forms.Button CandyStorebtn;
    }
}