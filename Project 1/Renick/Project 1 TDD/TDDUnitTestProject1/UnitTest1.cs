﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Project_1_TDD;

namespace TDDUnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Distancetest()
        {
            var lightyear = DistanceCalc.LightYearDist(58800000000);
            Assert.AreEqual(1, lightyear);
        }

        [TestMethod]
        public void Watertest()
        {
            var water = lifesupport.water(1);
            Assert.AreEqual(182.5, water);
        }

        [TestMethod]
        public void Oxygentest()
        {
            var oxygen = lifesupport.oxygen(1,1);
            Assert.AreEqual(148429.44, oxygen);
        }

    }
}
