﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shadowsocks.Encryption.CircularBuffer;

namespace test
{
    [TestClass]
    public class ByteCircularBuffer
    {
        [TestMethod]
        public void ByteCircularBuffer_CapacityTest1()
        {
            var bcb = new Shadowsocks.Encryption.CircularBuffer.ByteCircularBuffer(100);

            bcb.Capacity = 100;
            Assert.AreEqual(100, bcb.Capacity);
        }

        [TestMethod]
        public void ByteCircularBuffer_CapacityTest2()
        {
            var bcb = new Shadowsocks.Encryption.CircularBuffer.ByteCircularBuffer(100);

            bcb.Capacity = 1000;
            Assert.AreEqual(1000, bcb.Capacity);
        }

        [TestMethod]
        public void ByteCircularBuffer_CapacityTest3()
        {
            var bcb = new Shadowsocks.Encryption.CircularBuffer.ByteCircularBuffer(100);

            bcb.Capacity = 10;
            Assert.AreEqual(10, bcb.Capacity);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ByteCircularBuffer_CapacityTest4()
        {
            byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            var bcb = new Shadowsocks.Encryption.CircularBuffer.ByteCircularBuffer(100);

            bcb.Put(data);
            bcb.Capacity = 10;
        }
    }
}
