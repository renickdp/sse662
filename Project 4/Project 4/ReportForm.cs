﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Project_4
{
    public partial class ReportForm : Form
    {
        public ReportForm(Dictionary<string, List<Point<DateTime, int>>> points,
                          Color lineColor, Color backgroundColor,
                          int lineThickness, ReportMode mode)
        {
            InitializeComponent();
            this.reportChart.BackColor = backgroundColor;
            this.reportChart.ChartAreas[0].BackColor = backgroundColor;
            DateTime minX = DateTime.MaxValue;
            foreach (List<Point<DateTime, int>> pointList in points.Values)
            {
                foreach (Point<DateTime, int> point in pointList)
                {
                    if (point.x < minX)
                    {
                        minX = point.x;
                    }
                }
            }
            float lightnessOffset = 200 / points.Keys.Count;
            float hue = 240 *(lineColor.GetHue() / 360.0f);
            float saturation = lineColor.GetSaturation() * 240;
            float lightness = 20;

            foreach (string label in points.Keys)
            {
                Series series = new Series();
                series.XValueType = ChartValueType.DateTime;
                series.YValueType = ChartValueType.Int32;
                series.Name = label;
                series.ChartType = SeriesChartType.Line;
                Color color = (Color) new HSLColor(hue, saturation, lightness);
                lightness += lightnessOffset;
                series.Color = color;
                series.BorderWidth = lineThickness;

                List<Point<DateTime, int>> linePoints = points[label];
                if (linePoints[0].x != minX)
                {
                    series.Points.AddXY(minX, 0);
                }

                foreach (Point<DateTime, int> point in linePoints)
                {
                    series.Points.AddXY(point.x.ToOADate(), point.y);
                }
                
                this.reportChart.Series.Add(series);
            }
            this.reportChart.ChartAreas[0].AxisX.RoundAxisValues();

        }
    }
}
