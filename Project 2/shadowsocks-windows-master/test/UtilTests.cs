﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shadowsocks.Util;

namespace Sse662Tests
{
[TestClass]
public class UtilTests
{
    [TestMethod]
    public void GetBandwidthScaleTest1()
    {
        var bsi = Utils.GetBandwidthScale(0);
        Assert.AreEqual(0, bsi.value);
        Assert.AreEqual("B", bsi.unitName);
        Assert.AreEqual(1, bsi.unit);
    }
    [TestMethod]
    public void GetBandwidthScaleTest2()
    {
        var bsi = Utils.GetBandwidthScale(1);
        Assert.AreEqual((float)1, bsi.value);
        Assert.AreEqual("B", bsi.unitName);
        Assert.AreEqual(1, bsi.unit);
    }
    [TestMethod]
    public void GetBandwidthScaleTest3()
    {
        var bsi = Utils.GetBandwidthScale(1025);
        Assert.AreEqual(1, bsi.value, 0.01);
        Assert.AreEqual("KiB", bsi.unitName);
        Assert.AreEqual(1024, bsi.unit);
    }
    [TestMethod]
    public void GetBandwidthScaleTest4()
    {
        var bsi = Utils.GetBandwidthScale(1048577);
        Assert.AreEqual(1, bsi.value, 0.01);
        Assert.AreEqual("MiB", bsi.unitName);
        Assert.AreEqual(1048576, bsi.unit);
    }
    [TestMethod]
    public void GetBandwidthScaleTest5()
    {
        var bsi = Utils.GetBandwidthScale(1073741825);
        Assert.AreEqual(1024, bsi.value, 0.01);
        Assert.AreEqual("MiB", bsi.unitName);
        Assert.AreEqual(1048576, bsi.unit);
    }
    [TestMethod]
    public void GetBandwidthScaleTest6()
    {
        var bsi = Utils.GetBandwidthScale(1099511627777);
        Assert.AreEqual(1024, bsi.value, 0.01);
        Assert.AreEqual("GiB", bsi.unitName);
        Assert.AreEqual(1073741824, bsi.unit);
    }
}
}
