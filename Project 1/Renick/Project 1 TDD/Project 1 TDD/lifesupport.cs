﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_TDD
{
   public class lifesupport
    {

        public static double water(int passengers)
        {
            double watergallons = 0.0;

            var waterouncesday = 8 * 8;

            var waterouncenyear = waterouncesday * 365;

            double waterouncestotal = passengers * waterouncenyear;

            watergallons = waterouncestotal / 128;

            watergallons = watergallons * passengers;  

            return watergallons;
        }

        public static double oxygen(int passengers, double years)
        {
            double oxygen = 0.0;
            years = years * 365;

            oxygen = 8 * 60;

            oxygen = oxygen * 24;

            //liters value
            oxygen = oxygen * years;

            oxygen = oxygen * 0.0353;

            oxygen = oxygen * passengers;     

            return oxygen;
        }

    }
}
