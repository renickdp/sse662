﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_4
{
    public partial class Form1 : Form
    {
        private DataClasses1DataContext db;
        private CustomIterator<Inventory> inventoryIterator;
        private int nextPurchaseOrderNumber;
        private int nextOrderNumber;
        private TotalMediator orderTotalMediator;
        private TotalMediator poTotalMediator;
        private ReportBuilder reportBuilder;
        public Form1()
        {
            InitializeComponent();
            this.db = new DataClasses1DataContext();

            this.orderTotalMediator = new TotalMediator(orderSubTotalTextBox, orderTaxTextBox, orderTotalTextBox);
            this.poTotalMediator = new TotalMediator(SubtotalTxtBox, TaxTxtBox, TotalTxtBox);
            List<Inventory> inventories = this.db.Inventories.ToList();
            this.inventoryIterator = new CustomIterator<Inventory>(inventories);
            this.reportBuilder = new ReportBuilder();

            PurchaseOrder lastPO = this.db.PurchaseOrders.OrderByDescending(i => i.Id).FirstOrDefault();
            if (lastPO == null)
            {
                this.nextPurchaseOrderNumber = 1;
            }
            else
            {
                this.nextPurchaseOrderNumber = lastPO.OrderNumber + 1;
            }

            Order lastOrder = this.db.Orders.OrderByDescending(i => i.Id).FirstOrDefault();
            if (lastOrder == null)
            {
                this.nextOrderNumber = 1;
            }
            else
            {
                this.nextOrderNumber = lastOrder.OrderNumber + 1;
            }

            reportModeComboBox.DataSource = Enum.GetValues(typeof(ReportMode));

            // Preload the first group of inventory objects for user convenience
            this.viewMoreButtonClick(null, null);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'orders._Orders' table. You can move, or remove it, as needed.
            this.ordersTableAdapter.Fill(this.orders._Orders);
            // TODO: This line of code loads data into the 'purchaseOrderItemsForPO.PurchaseOrderItems' table. You can move, or remove it, as needed.
            this.purchaseOrderItemsTableAdapter.Fill(this.purchaseOrderItemsForPO.PurchaseOrderItems);
            // TODO: This line of code loads data into the 'purchaseOrders._PurchaseOrders' table. You can move, or remove it, as needed.
            this.purchaseOrdersTableAdapter.Fill(this.purchaseOrders._PurchaseOrders);
            // TODO: This line of code loads data into the 'items._Items' table. You can move, or remove it, as needed.
            this.itemsTableAdapter.Fill(this.items._Items);
        }

        private void OrdersLB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ItemsSelectBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = ((DataRowView)ItemsSelectBox.SelectedItem).Row;

            PriceTxtBox.Text = selectedItem["ItemPrice"].ToString();

        }

        private void viewMoreButtonClick(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {
                if (!this.inventoryIterator.hasnext())
                {
                    break;
                }
                Inventory inv = this.inventoryIterator.next();
                Item item = this.db.Items.Single(p => p.Id == inv.ProductId);
                string[] row = { item.ItemName, inv.Quantity.ToString(), item.ItemPrice.ToString() };
                this.inventoryDataGrid.Rows.Add(row);
            }

        }

        private void AddItemBtn_Click(object sender, EventArgs e)
        {
            int itemId = ((DataRowView)ItemsSelectBox.SelectedItem).Row.Field<int>("Id");
            int qty = (int)numericUpDown1.Value;

            PurchaseOrderItem pod = new PurchaseOrderItem
            {
                ItemId = itemId,
                OrderedAmount = qty,
                // Fake val. We'll fix it on save
                PurchaseOrderId = -1
            };
            PurchaseOrderItemsBox.Items.Add(pod);
            
            Item item = this.db.Items.Single(i => i.Id == itemId);

            this.poTotalMediator.incrementSubtotal(pod.OrderedAmount * item.ItemPrice);
        }

        private void CheckoutBtn_Click(object sender, EventArgs e)
        {
            PurchaseOrder po = new PurchaseOrder
            {
                OrderNumber = this.nextPurchaseOrderNumber
            };
            this.db.PurchaseOrders.InsertOnSubmit(po);
            this.db.SubmitChanges();

            this.nextPurchaseOrderNumber++;
            List<PurchaseOrderItem> pods = new List<PurchaseOrderItem>();
            foreach (PurchaseOrderItem pod in PurchaseOrderItemsBox.Items)
            {
                pod.PurchaseOrderId = po.Id;
                this.db.PurchaseOrderItems.InsertOnSubmit(pod);
            }
            this.db.SubmitChanges();
            PurchaseOrderItemsBox.Items.Clear();
            this.poTotalMediator.clear();
            //Refresh the purchase order dropdown in the other screen

            BindingSource updatedSource = new BindingSource();
            IQueryable<PurchaseOrder> pos = from p in this.db.PurchaseOrders
                                            select p;
            DataTable table = this.makePODataTable(pos);
            updatedSource.DataSource = table;
            purchaseOrderComboBox.DataSource = updatedSource;
        }

        private void receiveButton_Click(object sender, EventArgs e)
        {
            if (purchaseOrderComboBox.SelectedItem == null)
            {
                return;
            }
            foreach (PurchaseOrderItem poItem in purchaseOrdersListBox.Items)
            {
                Inventory existingInv = this.db.Inventories.SingleOrDefault(
                                                             i => i.Item == poItem.Item);
                Inventory inv;
                if (existingInv == null)
                {
                    inv = new Inventory
                    {
                        Item = poItem.Item,
                        Quantity = poItem.OrderedAmount
                    };
                    this.db.Inventories.InsertOnSubmit(inv);
                }
                else
                {
                    inv = existingInv;
                    existingInv.Quantity += poItem.OrderedAmount;
                }

                // The po has been received, so forget it
                this.db.PurchaseOrderItems.DeleteOnSubmit(poItem);
                this.db.SubmitChanges();
                InventoryChange change = new InventoryChange
                {
                    InventoryId = inv.Id,
                    QuantityChange = poItem.OrderedAmount,
                    Quantity = inv.Quantity
                };
                this.db.InventoryChanges.InsertOnSubmit(change);
                this.db.SubmitChanges();
            }
            // Delete the po after receipt
            DataRow row = ((DataRowView)purchaseOrderComboBox.SelectedItem).Row;
            int poId = row.Field<int>("Id");
            PurchaseOrder selectedPO = this.db.PurchaseOrders.Single(po => po.Id == poId);
            this.db.PurchaseOrders.DeleteOnSubmit(selectedPO);
            this.db.SubmitChanges();

            purchaseOrderComboBox.SelectedIndex = -1;
            purchaseOrdersListBox.Items.Clear();
            BindingSource updatedSource = new BindingSource();
            IQueryable<PurchaseOrder> pos = from p in this.db.PurchaseOrders
                                            select p;
            DataTable dt = this.makePODataTable(pos);
            updatedSource.DataSource = dt;
            purchaseOrderComboBox.DataSource = updatedSource;
            this.resetInventoryScreen();
        }

        private void ScreenTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage selectedTab = ScreenTabs.SelectedTab;
            if (selectedTab.Text == "Create Purchase Order")
            {
                this.ItemsSelectBox_SelectedIndexChanged(sender, e);
            }
            else if (selectedTab.Text == "Create Order")
            {
                this.OrderItemComboBox_SelectedIndexChanged(sender, e);
            }
            else if (selectedTab.Text == "Purchase Orders")
            {
                this.purchaseOrderComboBox_SelectedIndexChanged(sender, e);
            }
            else if (selectedTab.Text == "Orders")
            {
                this.orderSelectComboBox_SelectedIndexChanged(sender, e);
            }
        }

        private void purchaseOrderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (purchaseOrderComboBox.SelectedIndex == -1)
            {
                return;
            }
            purchaseOrdersListBox.Items.Clear();
            DataRow row = ((DataRowView)purchaseOrderComboBox.SelectedItem).Row;
            int poId = row.Field<int>("Id");
            PurchaseOrder selectedPO = this.db.PurchaseOrders.Single(po => po.Id == poId);
            IQueryable<PurchaseOrderItem> poItems = from poItem in this.db.PurchaseOrderItems
                                                    where poItem.PurchaseOrderId == poId
                                                    select poItem;

            foreach (PurchaseOrderItem pod in poItems)
            {
                purchaseOrdersListBox.Items.Add(pod);
            }
            purchaseOrdersListBox.Refresh();
        }

        private void orderSelectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (orderSelectComboBox.SelectedItem == null)
            {
                return;
            }
            orderDtlListBox.Items.Clear();
            DataRow row = ((DataRowView)orderSelectComboBox.SelectedItem).Row;
            int orderId = row.Field<int>("Id");
            Order selectedOrder = this.db.Orders.Single(order => order.Id == orderId);
            IQueryable<OrderedItem> dtls = from dtl in this.db.OrderedItems
                                           where dtl.OrderId == orderId
                                           select dtl;

            foreach (OrderedItem orderDtl in dtls)
            {
                orderDtlListBox.Items.Add(orderDtl);
            }
            orderDtlListBox.Refresh();
        }

        private void orderAddItemButton_Click(object sender, EventArgs e)
        {
            int itemId = ((DataRowView)OrderItemComboBox.SelectedItem).Row.Field<int>("Id");
            int qty = (int)orderNumberField.Value;

            OrderedItem odtl = new OrderedItem
            {
                ItemId = itemId,
                OrderedAmount = qty,
                // Fake val. We'll fix it on save
                OrderId = -1
            };
            CreateOrderedItemListBox.Items.Add(odtl);

            Item item = db.Items.Single(i => i.Id == itemId);

            this.orderTotalMediator.incrementSubtotal(odtl.OrderedAmount * item.ItemPrice);
        }

        private void OrderItemComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = ((DataRowView)OrderItemComboBox.SelectedItem).Row;

            OrderItemPriceTextBox.Text = selectedItem["ItemPrice"].ToString();
        }

        private void orderCheckOutButton_Click(object sender, EventArgs e)
        {
            Order order = new Order
            {
                OrderNumber = this.nextOrderNumber
            };
            this.db.Orders.InsertOnSubmit(order);
            this.db.SubmitChanges();

            this.nextOrderNumber++;
            List<OrderedItem> orderedItems = new List<OrderedItem>();
            foreach (OrderedItem orderedItem in CreateOrderedItemListBox.Items)
            {
                orderedItem.OrderId = order.Id;
                this.db.OrderedItems.InsertOnSubmit(orderedItem);
            }
            this.db.SubmitChanges();
            CreateOrderedItemListBox.Items.Clear();
            this.orderTotalMediator.clear();
            //Refresh the order dropdown in the other screen

            BindingSource updatedSource = new BindingSource();
            IQueryable<Order> orders = from ord in this.db.Orders
                                       select ord;
            DataTable table = this.makeOrderDataTable(orders);
            updatedSource.DataSource = table;
            orderSelectComboBox.DataSource = updatedSource;
        }
        private void resetInventoryScreen()
        {
            List<Inventory> inventories = this.db.Inventories.ToList();
            this.inventoryIterator = new CustomIterator<Inventory>(inventories);
            inventoryDataGrid.Rows.Clear();
            this.viewMoreButtonClick(null, null);
        }

        private DataTable makePODataTable(IQueryable<PurchaseOrder> pos)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("OrderNumber", typeof(string));
            foreach (PurchaseOrder po in pos)
            {
                DataRow row = dt.NewRow();
                row["Id"] = po.Id;
                row["OrderNumber"] = po.OrderNumber;
                dt.Rows.Add(row);
            }
            return dt;
        }
        private DataTable makeOrderDataTable(IQueryable<Order> orders)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("OrderNumber", typeof(string));
            foreach (Order order in orders)
            {
                DataRow row = dt.NewRow();
                row["Id"] = order.Id;
                row["OrderNumber"] = order.OrderNumber;
                dt.Rows.Add(row);
            }
            return dt;
        }

        private void packOrderButton_Click(object sender, EventArgs e)
        {
            if (orderSelectComboBox.SelectedItem == null)
            {
                return;
            }
            List<InventorySnapshot> invsChanged = new List<InventorySnapshot>();
            foreach (OrderedItem orderItem in orderDtlListBox.Items)
            {
                Item item = this.db.Items.Single<Item>(i => i.Id == orderItem.ItemId);
                Inventory existingInv = this.db.Inventories.SingleOrDefault(
                                                             i => i.Item == item);
                if (existingInv == null || existingInv.Quantity < orderItem.OrderedAmount)
                {
                    this.rollbackInventoryChanges(invsChanged);
                    this.db.SubmitChanges();
                    MessageBox.Show("Cannot fulfill order, not enough inventory");
                    return;
                }
                else
                {
                    object savedState = existingInv.getState();
                    invsChanged.Add(new InventorySnapshot(existingInv, savedState, -orderItem.OrderedAmount));
                    existingInv.Quantity -= orderItem.OrderedAmount;
                    this.db.SubmitChanges();
                }
            }

            foreach (InventorySnapshot snapshot in invsChanged)
            {
                InventoryChange change = new InventoryChange
                {
                    InventoryId = snapshot.inventory.Id,
                    QuantityChange = snapshot.quantityChange,
                    Quantity = snapshot.inventory.Quantity
                };
                this.db.InventoryChanges.InsertOnSubmit(change);
            }
            // Delete the order after fulfilling it

            DataRow row = ((DataRowView)orderSelectComboBox.SelectedItem).Row;
            int orderId = row.Field<int>("Id");
            Order selectedOrder = this.db.Orders.Single(o => o.Id == orderId);
            foreach (OrderedItem orderItem in orderDtlListBox.Items)
            {
                this.db.OrderedItems.DeleteOnSubmit(orderItem);
            }
            this.db.SubmitChanges();
            this.db.Orders.DeleteOnSubmit(selectedOrder);
            this.db.SubmitChanges();

            orderSelectComboBox.SelectedIndex = -1;
            orderDtlListBox.Items.Clear();
            BindingSource updatedSource = new BindingSource();
            IQueryable<Order> orders = from o in this.db.Orders
                                            select o;
            DataTable dt = this.makeOrderDataTable(orders);
            updatedSource.DataSource = dt;
            orderSelectComboBox.DataSource = updatedSource;
            this.resetInventoryScreen();
        }

        private void rollbackInventoryChanges(List<InventorySnapshot> invsChanged)
        {
            foreach (InventorySnapshot snapshot in invsChanged)
            {
                Inventory inv = snapshot.inventory;
                object state = snapshot.savedState;
                inv.setState(state);
            }
        }

        private void changeLineColorButton_Click(object sender, EventArgs e)
        {
            lineColorDialog.ShowDialog();
            Color color = lineColorDialog.Color;
            this.reportBuilder.setLineColor(color);
        }

        private void changeBackgroundColorButton_Click(object sender, EventArgs e)
        {
            backgroundColorDialog.ShowDialog();
            Color color = backgroundColorDialog.Color;
            this.reportBuilder.setBackgroundColor(color);
        }

        private void reportModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReportMode mode;
            Enum.TryParse<ReportMode>(reportModeComboBox.SelectedValue.ToString(), out mode);
            this.reportBuilder.setMode(mode);
        }

        private void lineThicknessUpDown_ValueChanged(object sender, EventArgs e)
        {
            int thickness = (int) this.lineThicknessUpDown.Value;
            this.reportBuilder.setLineThickness(thickness);
        }

        private void generateReportButton_Click(object sender, EventArgs e)
        {
            List<InventoryChange> changes = (from change in this.db.InventoryChanges
                                             select change).ToList();
            this.reportBuilder.addData(changes);
            Form form = this.reportBuilder.build();
            form.Show();
        }
    }
}
