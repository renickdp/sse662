﻿namespace Project_4
{
    public partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.inventoryDataGrid = new System.Windows.Forms.DataGridView();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AddItemBtn = new System.Windows.Forms.Button();
            this.ScreenTabs = new System.Windows.Forms.TabControl();
            this.inventoryTab = new System.Windows.Forms.TabPage();
            this.viewMoreButton = new System.Windows.Forms.Button();
            this.createPOTab = new System.Windows.Forms.TabPage();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.PriceTxtBox = new System.Windows.Forms.TextBox();
            this.CheckoutBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TotalTxtBox = new System.Windows.Forms.TextBox();
            this.TaxTxtBox = new System.Windows.Forms.TextBox();
            this.SubtotalTxtBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PurchaseOrderItemsBox = new System.Windows.Forms.ListBox();
            this.ItemsSelectBox = new System.Windows.Forms.ComboBox();
            this.itemsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.items = new Project_4.Items();
            this.purchaseOrdersTab = new System.Windows.Forms.TabPage();
            this.receiveButton = new System.Windows.Forms.Button();
            this.purchaseOrdersListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.purchaseOrderComboBox = new System.Windows.Forms.ComboBox();
            this.purchaseOrdersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purchaseOrders = new Project_4.PurchaseOrders();
            this.createOrderTab = new System.Windows.Forms.TabPage();
            this.orderNumberField = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.OrderItemPriceTextBox = new System.Windows.Forms.TextBox();
            this.orderCheckOutButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.orderTotalTextBox = new System.Windows.Forms.TextBox();
            this.orderTaxTextBox = new System.Windows.Forms.TextBox();
            this.orderSubTotalTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CreateOrderedItemListBox = new System.Windows.Forms.ListBox();
            this.OrderItemComboBox = new System.Windows.Forms.ComboBox();
            this.orderAddItemButton = new System.Windows.Forms.Button();
            this.ordersTab = new System.Windows.Forms.TabPage();
            this.packOrderButton = new System.Windows.Forms.Button();
            this.orderDtlListBox = new System.Windows.Forms.ListBox();
            this.label15 = new System.Windows.Forms.Label();
            this.orderSelectComboBox = new System.Windows.Forms.ComboBox();
            this.ordersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orders = new Project_4.Orders();
            this.reportsTab = new System.Windows.Forms.TabPage();
            this.generateReportButton = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lineThicknessUpDown = new System.Windows.Forms.NumericUpDown();
            this.reportModeComboBox = new System.Windows.Forms.ComboBox();
            this.changeBackgroundColorButton = new System.Windows.Forms.Button();
            this.changeLineColorButton = new System.Windows.Forms.Button();
            this.itemsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.itemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.itemsTableAdapter = new Project_4.ItemsTableAdapters.ItemsTableAdapter();
            this.tableAdapterManager = new Project_4.ItemsTableAdapters.TableAdapterManager();
            this.purchaseOrdersTableAdapter = new Project_4.PurchaseOrdersTableAdapters.PurchaseOrdersTableAdapter();
            this.tableAdapterManager1 = new Project_4.PurchaseOrdersTableAdapters.TableAdapterManager();
            this.purchaseOrderItemsForPO = new Project_4.PurchaseOrderItemsForPO();
            this.purchaseOrderItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purchaseOrderItemsTableAdapter = new Project_4.PurchaseOrderItemsForPOTableAdapters.PurchaseOrderItemsTableAdapter();
            this.ordersTableAdapter = new Project_4.OrdersTableAdapters.OrdersTableAdapter();
            this.lineColorDialog = new System.Windows.Forms.ColorDialog();
            this.backgroundColorDialog = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            this.ScreenTabs.SuspendLayout();
            this.inventoryTab.SuspendLayout();
            this.createPOTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.items)).BeginInit();
            this.purchaseOrdersTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrdersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrders)).BeginInit();
            this.createOrderTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderNumberField)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.ordersTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orders)).BeginInit();
            this.reportsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lineThicknessUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrderItemsForPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrderItemsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // inventoryDataGrid
            // 
            this.inventoryDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.inventoryDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemName,
            this.Qty,
            this.ItemPrice});
            this.inventoryDataGrid.Location = new System.Drawing.Point(0, 0);
            this.inventoryDataGrid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.inventoryDataGrid.Name = "inventoryDataGrid";
            this.inventoryDataGrid.Size = new System.Drawing.Size(638, 434);
            this.inventoryDataGrid.TabIndex = 0;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // Qty
            // 
            this.Qty.HeaderText = "Quantity";
            this.Qty.Name = "Qty";
            this.Qty.ReadOnly = true;
            // 
            // ItemPrice
            // 
            this.ItemPrice.HeaderText = "Item Price";
            this.ItemPrice.Name = "ItemPrice";
            this.ItemPrice.ReadOnly = true;
            // 
            // AddItemBtn
            // 
            this.AddItemBtn.Location = new System.Drawing.Point(324, 40);
            this.AddItemBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.AddItemBtn.Name = "AddItemBtn";
            this.AddItemBtn.Size = new System.Drawing.Size(112, 35);
            this.AddItemBtn.TabIndex = 1;
            this.AddItemBtn.Text = "Add Item";
            this.AddItemBtn.UseVisualStyleBackColor = true;
            this.AddItemBtn.Click += new System.EventHandler(this.AddItemBtn_Click);
            // 
            // ScreenTabs
            // 
            this.ScreenTabs.Controls.Add(this.inventoryTab);
            this.ScreenTabs.Controls.Add(this.createPOTab);
            this.ScreenTabs.Controls.Add(this.purchaseOrdersTab);
            this.ScreenTabs.Controls.Add(this.createOrderTab);
            this.ScreenTabs.Controls.Add(this.ordersTab);
            this.ScreenTabs.Controls.Add(this.reportsTab);
            this.ScreenTabs.Location = new System.Drawing.Point(60, 118);
            this.ScreenTabs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ScreenTabs.Name = "ScreenTabs";
            this.ScreenTabs.SelectedIndex = 0;
            this.ScreenTabs.ShowToolTips = true;
            this.ScreenTabs.Size = new System.Drawing.Size(840, 460);
            this.ScreenTabs.TabIndex = 2;
            this.ScreenTabs.SelectedIndexChanged += new System.EventHandler(this.ScreenTabs_SelectedIndexChanged);
            // 
            // inventoryTab
            // 
            this.inventoryTab.Controls.Add(this.viewMoreButton);
            this.inventoryTab.Controls.Add(this.inventoryDataGrid);
            this.inventoryTab.Location = new System.Drawing.Point(4, 29);
            this.inventoryTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.inventoryTab.Name = "inventoryTab";
            this.inventoryTab.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.inventoryTab.Size = new System.Drawing.Size(832, 427);
            this.inventoryTab.TabIndex = 0;
            this.inventoryTab.Text = "Inventory";
            this.inventoryTab.UseVisualStyleBackColor = true;
            // 
            // viewMoreButton
            // 
            this.viewMoreButton.Location = new System.Drawing.Point(686, 50);
            this.viewMoreButton.Name = "viewMoreButton";
            this.viewMoreButton.Size = new System.Drawing.Size(105, 49);
            this.viewMoreButton.TabIndex = 1;
            this.viewMoreButton.Text = "View More";
            this.viewMoreButton.UseVisualStyleBackColor = true;
            this.viewMoreButton.Click += new System.EventHandler(this.viewMoreButtonClick);
            // 
            // createPOTab
            // 
            this.createPOTab.AutoScroll = true;
            this.createPOTab.BackColor = System.Drawing.Color.Transparent;
            this.createPOTab.Controls.Add(this.numericUpDown1);
            this.createPOTab.Controls.Add(this.label5);
            this.createPOTab.Controls.Add(this.PriceTxtBox);
            this.createPOTab.Controls.Add(this.CheckoutBtn);
            this.createPOTab.Controls.Add(this.groupBox1);
            this.createPOTab.Controls.Add(this.label3);
            this.createPOTab.Controls.Add(this.label2);
            this.createPOTab.Controls.Add(this.PurchaseOrderItemsBox);
            this.createPOTab.Controls.Add(this.ItemsSelectBox);
            this.createPOTab.Controls.Add(this.AddItemBtn);
            this.createPOTab.Location = new System.Drawing.Point(4, 29);
            this.createPOTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.createPOTab.Name = "createPOTab";
            this.createPOTab.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.createPOTab.Size = new System.Drawing.Size(832, 427);
            this.createPOTab.TabIndex = 1;
            this.createPOTab.Text = "Create Purchase Order";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(232, 46);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(56, 26);
            this.numericUpDown1.TabIndex = 11;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(159, 11);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Price:";
            // 
            // PriceTxtBox
            // 
            this.PriceTxtBox.Location = new System.Drawing.Point(159, 45);
            this.PriceTxtBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PriceTxtBox.Name = "PriceTxtBox";
            this.PriceTxtBox.Size = new System.Drawing.Size(66, 26);
            this.PriceTxtBox.TabIndex = 9;
            // 
            // CheckoutBtn
            // 
            this.CheckoutBtn.Location = new System.Drawing.Point(662, 375);
            this.CheckoutBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CheckoutBtn.Name = "CheckoutBtn";
            this.CheckoutBtn.Size = new System.Drawing.Size(112, 35);
            this.CheckoutBtn.TabIndex = 8;
            this.CheckoutBtn.Text = "Checkout";
            this.CheckoutBtn.UseVisualStyleBackColor = true;
            this.CheckoutBtn.Click += new System.EventHandler(this.CheckoutBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TotalTxtBox);
            this.groupBox1.Controls.Add(this.TaxTxtBox);
            this.groupBox1.Controls.Add(this.SubtotalTxtBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(489, 143);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(285, 225);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Totals";
            // 
            // TotalTxtBox
            // 
            this.TotalTxtBox.Location = new System.Drawing.Point(126, 162);
            this.TotalTxtBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TotalTxtBox.Name = "TotalTxtBox";
            this.TotalTxtBox.Size = new System.Drawing.Size(148, 26);
            this.TotalTxtBox.TabIndex = 5;
            // 
            // TaxTxtBox
            // 
            this.TaxTxtBox.Location = new System.Drawing.Point(126, 100);
            this.TaxTxtBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TaxTxtBox.Name = "TaxTxtBox";
            this.TaxTxtBox.Size = new System.Drawing.Size(148, 26);
            this.TaxTxtBox.TabIndex = 4;
            // 
            // SubtotalTxtBox
            // 
            this.SubtotalTxtBox.Location = new System.Drawing.Point(126, 43);
            this.SubtotalTxtBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SubtotalTxtBox.Name = "SubtotalTxtBox";
            this.SubtotalTxtBox.Size = new System.Drawing.Size(148, 26);
            this.SubtotalTxtBox.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(66, 166);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(75, 105);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Tax";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 48);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Subtotal";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Qty:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Item:";
            // 
            // PurchaseOrderItemsBox
            // 
            this.PurchaseOrderItemsBox.FormattingEnabled = true;
            this.PurchaseOrderItemsBox.ItemHeight = 20;
            this.PurchaseOrderItemsBox.Location = new System.Drawing.Point(33, 157);
            this.PurchaseOrderItemsBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PurchaseOrderItemsBox.Name = "PurchaseOrderItemsBox";
            this.PurchaseOrderItemsBox.Size = new System.Drawing.Size(292, 204);
            this.PurchaseOrderItemsBox.TabIndex = 4;
            this.PurchaseOrderItemsBox.SelectedIndexChanged += new System.EventHandler(this.OrderItemComboBox_SelectedIndexChanged);
            // 
            // ItemsSelectBox
            // 
            this.ItemsSelectBox.DataSource = this.itemsBindingSource2;
            this.ItemsSelectBox.DisplayMember = "ItemName";
            this.ItemsSelectBox.FormattingEnabled = true;
            this.ItemsSelectBox.Location = new System.Drawing.Point(33, 43);
            this.ItemsSelectBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ItemsSelectBox.Name = "ItemsSelectBox";
            this.ItemsSelectBox.Size = new System.Drawing.Size(115, 28);
            this.ItemsSelectBox.TabIndex = 2;
            this.ItemsSelectBox.ValueMember = "Id";
            this.ItemsSelectBox.SelectedIndexChanged += new System.EventHandler(this.ItemsSelectBox_SelectedIndexChanged);
            // 
            // itemsBindingSource2
            // 
            this.itemsBindingSource2.DataMember = "Items";
            this.itemsBindingSource2.DataSource = this.items;
            // 
            // items
            // 
            this.items.DataSetName = "Items";
            this.items.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // purchaseOrdersTab
            // 
            this.purchaseOrdersTab.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.purchaseOrdersTab.Controls.Add(this.receiveButton);
            this.purchaseOrdersTab.Controls.Add(this.purchaseOrdersListBox);
            this.purchaseOrdersTab.Controls.Add(this.label1);
            this.purchaseOrdersTab.Controls.Add(this.purchaseOrderComboBox);
            this.purchaseOrdersTab.Location = new System.Drawing.Point(4, 29);
            this.purchaseOrdersTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.purchaseOrdersTab.Name = "purchaseOrdersTab";
            this.purchaseOrdersTab.Size = new System.Drawing.Size(832, 427);
            this.purchaseOrdersTab.TabIndex = 2;
            this.purchaseOrdersTab.Text = "Purchase Orders";
            // 
            // receiveButton
            // 
            this.receiveButton.Location = new System.Drawing.Point(408, 106);
            this.receiveButton.Name = "receiveButton";
            this.receiveButton.Size = new System.Drawing.Size(156, 45);
            this.receiveButton.TabIndex = 4;
            this.receiveButton.Text = "Receive";
            this.receiveButton.UseVisualStyleBackColor = true;
            this.receiveButton.Click += new System.EventHandler(this.receiveButton_Click);
            // 
            // purchaseOrdersListBox
            // 
            this.purchaseOrdersListBox.FormattingEnabled = true;
            this.purchaseOrdersListBox.ItemHeight = 20;
            this.purchaseOrdersListBox.Location = new System.Drawing.Point(32, 106);
            this.purchaseOrdersListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.purchaseOrdersListBox.Name = "purchaseOrdersListBox";
            this.purchaseOrdersListBox.Size = new System.Drawing.Size(355, 304);
            this.purchaseOrdersListBox.TabIndex = 3;
            this.purchaseOrdersListBox.SelectedIndexChanged += new System.EventHandler(this.OrdersLB_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Purchase order Number:";
            // 
            // purchaseOrderComboBox
            // 
            this.purchaseOrderComboBox.DataSource = this.purchaseOrdersBindingSource;
            this.purchaseOrderComboBox.DisplayMember = "OrderNumber";
            this.purchaseOrderComboBox.FormattingEnabled = true;
            this.purchaseOrderComboBox.Location = new System.Drawing.Point(242, 23);
            this.purchaseOrderComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.purchaseOrderComboBox.Name = "purchaseOrderComboBox";
            this.purchaseOrderComboBox.Size = new System.Drawing.Size(180, 28);
            this.purchaseOrderComboBox.TabIndex = 0;
            this.purchaseOrderComboBox.ValueMember = "Id";
            this.purchaseOrderComboBox.SelectedIndexChanged += new System.EventHandler(this.purchaseOrderComboBox_SelectedIndexChanged);
            // 
            // purchaseOrdersBindingSource
            // 
            this.purchaseOrdersBindingSource.DataMember = "PurchaseOrders";
            this.purchaseOrdersBindingSource.DataSource = this.purchaseOrders;
            // 
            // purchaseOrders
            // 
            this.purchaseOrders.DataSetName = "PurchaseOrders";
            this.purchaseOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // createOrderTab
            // 
            this.createOrderTab.Controls.Add(this.orderNumberField);
            this.createOrderTab.Controls.Add(this.label9);
            this.createOrderTab.Controls.Add(this.OrderItemPriceTextBox);
            this.createOrderTab.Controls.Add(this.orderCheckOutButton);
            this.createOrderTab.Controls.Add(this.groupBox2);
            this.createOrderTab.Controls.Add(this.label13);
            this.createOrderTab.Controls.Add(this.label14);
            this.createOrderTab.Controls.Add(this.CreateOrderedItemListBox);
            this.createOrderTab.Controls.Add(this.OrderItemComboBox);
            this.createOrderTab.Controls.Add(this.orderAddItemButton);
            this.createOrderTab.Location = new System.Drawing.Point(4, 29);
            this.createOrderTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.createOrderTab.Name = "createOrderTab";
            this.createOrderTab.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.createOrderTab.Size = new System.Drawing.Size(832, 427);
            this.createOrderTab.TabIndex = 6;
            this.createOrderTab.Text = "Create Order";
            // 
            // orderNumberField
            // 
            this.orderNumberField.Location = new System.Drawing.Point(233, 54);
            this.orderNumberField.Name = "orderNumberField";
            this.orderNumberField.Size = new System.Drawing.Size(56, 26);
            this.orderNumberField.TabIndex = 21;
            this.orderNumberField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(164, 24);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 20);
            this.label9.TabIndex = 20;
            this.label9.Text = "Price:";
            // 
            // OrderItemPriceTextBox
            // 
            this.OrderItemPriceTextBox.Location = new System.Drawing.Point(160, 53);
            this.OrderItemPriceTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OrderItemPriceTextBox.Name = "OrderItemPriceTextBox";
            this.OrderItemPriceTextBox.Size = new System.Drawing.Size(66, 26);
            this.OrderItemPriceTextBox.TabIndex = 19;
            // 
            // orderCheckOutButton
            // 
            this.orderCheckOutButton.Location = new System.Drawing.Point(663, 383);
            this.orderCheckOutButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderCheckOutButton.Name = "orderCheckOutButton";
            this.orderCheckOutButton.Size = new System.Drawing.Size(112, 35);
            this.orderCheckOutButton.TabIndex = 18;
            this.orderCheckOutButton.Text = "Create Order";
            this.orderCheckOutButton.UseVisualStyleBackColor = true;
            this.orderCheckOutButton.Click += new System.EventHandler(this.orderCheckOutButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.orderTotalTextBox);
            this.groupBox2.Controls.Add(this.orderTaxTextBox);
            this.groupBox2.Controls.Add(this.orderSubTotalTextBox);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(490, 151);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(285, 225);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Totals";
            // 
            // orderTotalTextBox
            // 
            this.orderTotalTextBox.Location = new System.Drawing.Point(126, 162);
            this.orderTotalTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderTotalTextBox.Name = "orderTotalTextBox";
            this.orderTotalTextBox.Size = new System.Drawing.Size(148, 26);
            this.orderTotalTextBox.TabIndex = 5;
            // 
            // orderTaxTextBox
            // 
            this.orderTaxTextBox.Location = new System.Drawing.Point(126, 100);
            this.orderTaxTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderTaxTextBox.Name = "orderTaxTextBox";
            this.orderTaxTextBox.Size = new System.Drawing.Size(148, 26);
            this.orderTaxTextBox.TabIndex = 4;
            // 
            // orderSubTotalTextBox
            // 
            this.orderSubTotalTextBox.Location = new System.Drawing.Point(126, 43);
            this.orderSubTotalTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderSubTotalTextBox.Name = "orderSubTotalTextBox";
            this.orderSubTotalTextBox.Size = new System.Drawing.Size(148, 26);
            this.orderSubTotalTextBox.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(66, 166);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Total";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(75, 105);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "Tax";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(44, 48);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "Subtotal";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(245, 24);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 20);
            this.label13.TabIndex = 16;
            this.label13.Text = "Qty:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(38, 24);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 20);
            this.label14.TabIndex = 15;
            this.label14.Text = "Item:";
            // 
            // CreateOrderedItemListBox
            // 
            this.CreateOrderedItemListBox.FormattingEnabled = true;
            this.CreateOrderedItemListBox.ItemHeight = 20;
            this.CreateOrderedItemListBox.Location = new System.Drawing.Point(34, 165);
            this.CreateOrderedItemListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CreateOrderedItemListBox.Name = "CreateOrderedItemListBox";
            this.CreateOrderedItemListBox.Size = new System.Drawing.Size(292, 204);
            this.CreateOrderedItemListBox.TabIndex = 14;
            // 
            // OrderItemComboBox
            // 
            this.OrderItemComboBox.DataSource = this.itemsBindingSource2;
            this.OrderItemComboBox.DisplayMember = "ItemName";
            this.OrderItemComboBox.FormattingEnabled = true;
            this.OrderItemComboBox.Location = new System.Drawing.Point(34, 51);
            this.OrderItemComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OrderItemComboBox.Name = "OrderItemComboBox";
            this.OrderItemComboBox.Size = new System.Drawing.Size(115, 28);
            this.OrderItemComboBox.TabIndex = 13;
            this.OrderItemComboBox.ValueMember = "Id";
            this.OrderItemComboBox.SelectedIndexChanged += new System.EventHandler(this.OrderItemComboBox_SelectedIndexChanged);
            // 
            // orderAddItemButton
            // 
            this.orderAddItemButton.Location = new System.Drawing.Point(325, 48);
            this.orderAddItemButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderAddItemButton.Name = "orderAddItemButton";
            this.orderAddItemButton.Size = new System.Drawing.Size(112, 35);
            this.orderAddItemButton.TabIndex = 12;
            this.orderAddItemButton.Text = "Add Item";
            this.orderAddItemButton.UseVisualStyleBackColor = true;
            this.orderAddItemButton.Click += new System.EventHandler(this.orderAddItemButton_Click);
            // 
            // ordersTab
            // 
            this.ordersTab.Controls.Add(this.packOrderButton);
            this.ordersTab.Controls.Add(this.orderDtlListBox);
            this.ordersTab.Controls.Add(this.label15);
            this.ordersTab.Controls.Add(this.orderSelectComboBox);
            this.ordersTab.Location = new System.Drawing.Point(4, 29);
            this.ordersTab.Name = "ordersTab";
            this.ordersTab.Padding = new System.Windows.Forms.Padding(3);
            this.ordersTab.Size = new System.Drawing.Size(832, 427);
            this.ordersTab.TabIndex = 5;
            this.ordersTab.Text = "Orders";
            this.ordersTab.UseVisualStyleBackColor = true;
            // 
            // packOrderButton
            // 
            this.packOrderButton.Location = new System.Drawing.Point(389, 103);
            this.packOrderButton.Name = "packOrderButton";
            this.packOrderButton.Size = new System.Drawing.Size(147, 53);
            this.packOrderButton.TabIndex = 8;
            this.packOrderButton.Text = "Pack";
            this.packOrderButton.UseVisualStyleBackColor = true;
            this.packOrderButton.Click += new System.EventHandler(this.packOrderButton_Click);
            // 
            // orderDtlListBox
            // 
            this.orderDtlListBox.FormattingEnabled = true;
            this.orderDtlListBox.ItemHeight = 20;
            this.orderDtlListBox.Location = new System.Drawing.Point(27, 103);
            this.orderDtlListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderDtlListBox.Name = "orderDtlListBox";
            this.orderDtlListBox.Size = new System.Drawing.Size(355, 304);
            this.orderDtlListBox.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(162, 20);
            this.label15.TabIndex = 6;
            this.label15.Text = "Select Order Number:";
            // 
            // orderSelectComboBox
            // 
            this.orderSelectComboBox.DataSource = this.ordersBindingSource;
            this.orderSelectComboBox.DisplayMember = "OrderNumber";
            this.orderSelectComboBox.FormattingEnabled = true;
            this.orderSelectComboBox.Location = new System.Drawing.Point(178, 20);
            this.orderSelectComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderSelectComboBox.Name = "orderSelectComboBox";
            this.orderSelectComboBox.Size = new System.Drawing.Size(180, 28);
            this.orderSelectComboBox.TabIndex = 5;
            this.orderSelectComboBox.ValueMember = "Id";
            this.orderSelectComboBox.SelectedIndexChanged += new System.EventHandler(this.orderSelectComboBox_SelectedIndexChanged);
            // 
            // ordersBindingSource
            // 
            this.ordersBindingSource.DataMember = "Orders";
            this.ordersBindingSource.DataSource = this.orders;
            // 
            // orders
            // 
            this.orders.DataSetName = "Orders";
            this.orders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportsTab
            // 
            this.reportsTab.Controls.Add(this.generateReportButton);
            this.reportsTab.Controls.Add(this.label17);
            this.reportsTab.Controls.Add(this.label16);
            this.reportsTab.Controls.Add(this.lineThicknessUpDown);
            this.reportsTab.Controls.Add(this.reportModeComboBox);
            this.reportsTab.Controls.Add(this.changeBackgroundColorButton);
            this.reportsTab.Controls.Add(this.changeLineColorButton);
            this.reportsTab.Location = new System.Drawing.Point(4, 29);
            this.reportsTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.reportsTab.Name = "reportsTab";
            this.reportsTab.Size = new System.Drawing.Size(832, 427);
            this.reportsTab.TabIndex = 4;
            this.reportsTab.Text = "Reports";
            this.reportsTab.UseVisualStyleBackColor = true;
            // 
            // generateReportButton
            // 
            this.generateReportButton.Location = new System.Drawing.Point(326, 255);
            this.generateReportButton.Name = "generateReportButton";
            this.generateReportButton.Size = new System.Drawing.Size(159, 38);
            this.generateReportButton.TabIndex = 7;
            this.generateReportButton.Text = "Generate Report";
            this.generateReportButton.UseVisualStyleBackColor = true;
            this.generateReportButton.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(322, 176);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 20);
            this.label17.TabIndex = 6;
            this.label17.Text = "Mode";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(257, 208);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 20);
            this.label16.TabIndex = 5;
            this.label16.Text = "Line Thickness";
            // 
            // lineThicknessUpDown
            // 
            this.lineThicknessUpDown.Location = new System.Drawing.Point(377, 206);
            this.lineThicknessUpDown.Name = "lineThicknessUpDown";
            this.lineThicknessUpDown.Size = new System.Drawing.Size(120, 26);
            this.lineThicknessUpDown.TabIndex = 4;
            this.lineThicknessUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lineThicknessUpDown.ValueChanged += new System.EventHandler(this.lineThicknessUpDown_ValueChanged);
            // 
            // reportModeComboBox
            // 
            this.reportModeComboBox.FormattingEnabled = true;
            this.reportModeComboBox.Location = new System.Drawing.Point(377, 173);
            this.reportModeComboBox.Name = "reportModeComboBox";
            this.reportModeComboBox.Size = new System.Drawing.Size(121, 28);
            this.reportModeComboBox.TabIndex = 3;
            this.reportModeComboBox.SelectedIndexChanged += new System.EventHandler(this.reportModeComboBox_SelectedIndexChanged);
            // 
            // changeBackgroundColorButton
            // 
            this.changeBackgroundColorButton.Location = new System.Drawing.Point(311, 119);
            this.changeBackgroundColorButton.Name = "changeBackgroundColorButton";
            this.changeBackgroundColorButton.Size = new System.Drawing.Size(187, 48);
            this.changeBackgroundColorButton.TabIndex = 2;
            this.changeBackgroundColorButton.Text = "Change Background Color";
            this.changeBackgroundColorButton.UseVisualStyleBackColor = true;
            this.changeBackgroundColorButton.Click += new System.EventHandler(this.changeBackgroundColorButton_Click);
            // 
            // changeLineColorButton
            // 
            this.changeLineColorButton.Location = new System.Drawing.Point(311, 65);
            this.changeLineColorButton.Name = "changeLineColorButton";
            this.changeLineColorButton.Size = new System.Drawing.Size(187, 48);
            this.changeLineColorButton.TabIndex = 1;
            this.changeLineColorButton.Text = "Change Line Color";
            this.changeLineColorButton.UseVisualStyleBackColor = true;
            this.changeLineColorButton.Click += new System.EventHandler(this.changeLineColorButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(66, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(587, 55);
            this.label4.TabIndex = 3;
            this.label4.Text = "Patterns Inventory System";
            // 
            // itemsBindingSource
            // 
            this.itemsBindingSource.DataSource = this.items;
            this.itemsBindingSource.Position = 0;
            // 
            // itemsTableAdapter
            // 
            this.itemsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ItemsTableAdapter = this.itemsTableAdapter;
            this.tableAdapterManager.UpdateOrder = Project_4.ItemsTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // purchaseOrdersTableAdapter
            // 
            this.purchaseOrdersTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.PurchaseOrdersTableAdapter = this.purchaseOrdersTableAdapter;
            this.tableAdapterManager1.UpdateOrder = Project_4.PurchaseOrdersTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // purchaseOrderItemsForPO
            // 
            this.purchaseOrderItemsForPO.DataSetName = "PurchaseOrderItemsForPO";
            this.purchaseOrderItemsForPO.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // purchaseOrderItemsBindingSource
            // 
            this.purchaseOrderItemsBindingSource.DataMember = "PurchaseOrderItems";
            this.purchaseOrderItemsBindingSource.DataSource = this.purchaseOrderItemsForPO;
            // 
            // purchaseOrderItemsTableAdapter
            // 
            this.purchaseOrderItemsTableAdapter.ClearBeforeFill = true;
            // 
            // ordersTableAdapter
            // 
            this.ordersTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 631);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ScreenTabs);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.inventoryDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            this.ScreenTabs.ResumeLayout(false);
            this.inventoryTab.ResumeLayout(false);
            this.createPOTab.ResumeLayout(false);
            this.createPOTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.items)).EndInit();
            this.purchaseOrdersTab.ResumeLayout(false);
            this.purchaseOrdersTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrdersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrders)).EndInit();
            this.createOrderTab.ResumeLayout(false);
            this.createOrderTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderNumberField)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ordersTab.ResumeLayout(false);
            this.ordersTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orders)).EndInit();
            this.reportsTab.ResumeLayout(false);
            this.reportsTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lineThicknessUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrderItemsForPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseOrderItemsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView inventoryDataGrid;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inStockDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button AddItemBtn;
        private System.Windows.Forms.TabControl ScreenTabs;
        private System.Windows.Forms.TabPage inventoryTab;
        private System.Windows.Forms.TabPage createPOTab;
        private System.Windows.Forms.TabPage purchaseOrdersTab;
        private System.Windows.Forms.TabPage reportsTab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox purchaseOrderComboBox;
        public System.Windows.Forms.ListBox purchaseOrdersListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox PurchaseOrderItemsBox;
        private System.Windows.Forms.ComboBox ItemsSelectBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CheckoutBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox PriceTxtBox;
        private System.Windows.Forms.BindingSource itemsBindingSource1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TotalTxtBox;
        private System.Windows.Forms.TextBox TaxTxtBox;
        private System.Windows.Forms.TextBox SubtotalTxtBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button viewMoreButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemPrice;
        private System.Windows.Forms.BindingSource itemsBindingSource;
        private Items items;
        private System.Windows.Forms.BindingSource itemsBindingSource2;
        private ItemsTableAdapters.ItemsTableAdapter itemsTableAdapter;
        private ItemsTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button receiveButton;
        private PurchaseOrders purchaseOrders;
        private System.Windows.Forms.BindingSource purchaseOrdersBindingSource;
        private PurchaseOrdersTableAdapters.PurchaseOrdersTableAdapter purchaseOrdersTableAdapter;
        private PurchaseOrdersTableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TabPage ordersTab;
        private System.Windows.Forms.TabPage createOrderTab;
        private System.Windows.Forms.NumericUpDown orderNumberField;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox OrderItemPriceTextBox;
        private System.Windows.Forms.Button orderCheckOutButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox orderTotalTextBox;
        private System.Windows.Forms.TextBox orderTaxTextBox;
        private System.Windows.Forms.TextBox orderSubTotalTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListBox CreateOrderedItemListBox;
        private System.Windows.Forms.ComboBox OrderItemComboBox;
        private System.Windows.Forms.Button orderAddItemButton;
        private System.Windows.Forms.Button packOrderButton;
        public System.Windows.Forms.ListBox orderDtlListBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox orderSelectComboBox;
        private PurchaseOrderItemsForPO purchaseOrderItemsForPO;
        private System.Windows.Forms.BindingSource purchaseOrderItemsBindingSource;
        private PurchaseOrderItemsForPOTableAdapters.PurchaseOrderItemsTableAdapter purchaseOrderItemsTableAdapter;
        private Orders orders;
        private System.Windows.Forms.BindingSource ordersBindingSource;
        private OrdersTableAdapters.OrdersTableAdapter ordersTableAdapter;
        private System.Windows.Forms.Button changeLineColorButton;
        private System.Windows.Forms.Button generateReportButton;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown lineThicknessUpDown;
        private System.Windows.Forms.ComboBox reportModeComboBox;
        private System.Windows.Forms.Button changeBackgroundColorButton;
        private System.Windows.Forms.ColorDialog lineColorDialog;
        private System.Windows.Forms.ColorDialog backgroundColorDialog;
    }
}

