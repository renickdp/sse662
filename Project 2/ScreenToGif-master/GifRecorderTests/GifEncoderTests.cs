﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using ScreenToGif;
using ScreenToGif.Encoding;

namespace GifRecorderTests
{
    [TestClass]
    public class GifEncoderTests
    {
        private const string IMG_FOLDER = @"..\..\images";

        private List<Bitmap> GetTestFrames()
        {
            List<Bitmap> list = new List<Bitmap>();

            list.Add(new Bitmap(Path.Combine(IMG_FOLDER, "blue_small.png")));
            list.Add(new Bitmap(Path.Combine(IMG_FOLDER, "green_small.png")));

            return list;
        }

        [TestMethod]
        public void GifEncoder_AddFrameTest1()
        {
            List<Bitmap> frames = GetTestFrames();

            using(var mStream = new MemoryStream())
            {
                using(var encoder = new GifEncoder(mStream, null, null, -1))
                {
                    foreach (var frame in frames)
                        encoder.AddFrame(frame, TimeSpan.FromMilliseconds(100));
                }
                
                mStream.Position = 0;                
                byte[] buf = new byte[1024];
                long len = mStream.Length;
                int read = mStream.Read(buf, 0, 1024);

                Assert.AreEqual(71, buf[0]);
                Assert.AreEqual(247, buf[10]);
                Assert.AreEqual(153, buf[400]);
                Assert.AreEqual(51, buf[1000]);

                using(var fStream = new FileStream(Path.Combine(IMG_FOLDER, "test.gif"), 
                    FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    mStream.WriteTo(fStream);
                }
            }
        }

        [TestMethod]
        public void GifEncoder_AddFrameTest2()
        {
            var file = Path.Combine(IMG_FOLDER, "test2.gif");
            var form = new Legacy();

            var frames = form.GetFrameList();
            var delays = form.GetDelayList();
            form.SetOutputPath(file);

            frames.Add(Path.Combine(IMG_FOLDER, "blue_small.png"));
            frames.Add(Path.Combine(IMG_FOLDER, "green_small.png"));
            delays.Add(100);
            delays.Add(100);

            form.DoSave();

            Thread.Sleep(1000);

            using (var fStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                byte[] buf = new byte[1024];
                long len = fStream.Length;
                int read = fStream.Read(buf, 0, 1024);

                Assert.AreEqual(71, buf[0]);
                Assert.AreEqual(247, buf[10]);
                Assert.AreEqual(153, buf[400]);
                Assert.AreEqual(51, buf[1000]);
            }
        }
    }

    public static class TestExtensions
    {
        public static List<string> GetFrameList(this Legacy form)
        {
            PrivateObject po = new PrivateObject(form);
            return (List<string>)po.GetField("_listFrames");
        }

        public static List<int> GetDelayList(this Legacy form)
        {
            PrivateObject po = new PrivateObject(form);
            return (List<int>)po.GetField("_listDelay");
        }

        public static void SetOutputPath(this Legacy form, string value)
        {
            PrivateObject po = new PrivateObject(form);
            po.SetField("_outputpath", value);
        }

        public static void DoSave(this Legacy form)
        {
            PrivateObject po = new PrivateObject(form);
            po.Invoke("DoWork", null);
        }
    }
}
