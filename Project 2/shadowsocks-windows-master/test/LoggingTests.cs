﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shadowsocks.Controller;

namespace Sse662Tests
{
    [TestClass]
    public class LoggingTests
    {
        [TestMethod]
        public void StreamWriterWithTimestamp_WriteLineTest()
        {
            FileStream fs = new FileStream("temp.txt", FileMode.Create);
            var test = new StreamWriterWithTimestamp(fs);

            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            test.WriteLine("test");

            test.Close();
            test.Dispose();
            fs.Close();
            fs.Dispose();

            using(fs = new FileStream("temp.txt", FileMode.Open))
            using(StreamReader sr = new StreamReader(fs))
            {
                string result = sr.ReadLine();
                Assert.AreEqual(String.Format("[{0}] test", time), result);
            }
        }

        [TestMethod]
        public void StreamWriterWithTimestamp_WriteTest()
        {
            FileStream fs = new FileStream("temp.txt", FileMode.Create);
            var test = new StreamWriterWithTimestamp(fs);

            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            test.Write("test");

            test.Close();
            test.Dispose();
            fs.Close();
            fs.Dispose();

            using (fs = new FileStream("temp.txt", FileMode.Open))
            using (StreamReader sr = new StreamReader(fs))
            {
                string result = sr.ReadLine();
                Assert.AreEqual(String.Format("[{0}] test", time), result);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedAccessException))]
        public void LoggingTest1()
        {
            Logging.OpenLogFile();

            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Logging.Info("test");

            using (var fs = new FileStream("ss_win_temp\\shadowsocks.log", FileMode.Open))
            using (StreamReader sr = new StreamReader(fs))
            {
                string result = sr.ReadLine();
                Assert.AreEqual(String.Format("[{0}] test", time), result);
            }

            Logging.Clear();

            using (var fs = new FileStream("ss_win_temp\\shadowsocks.log", FileMode.Open))
            using (StreamReader sr = new StreamReader(fs))
            {
                string result = sr.ReadLine();
                Assert.AreEqual(true, String.IsNullOrEmpty(result));
            }
        }
    }
}
