﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{
    public abstract class WindowFactory
    {
        abstract public Form CreateStoreForm();
        abstract public AbstractReceipt CreateReceiptForm(List<string> lines);

    }

    public class FoodWindowFactory : WindowFactory
    {
        public override Form CreateStoreForm()
        {
            return new Form1(this);
        }
        public override AbstractReceipt CreateReceiptForm(List<string> lines)
        {
            return new FoodReceipt(this, lines);
        }

    }

    public class CandyWindowFactory : WindowFactory 
    {
        public override Form CreateStoreForm()
        {
            return new Form2(this);
        }
        public override AbstractReceipt CreateReceiptForm(List<string> lines)
        {
            return new CandyReceipt(this, lines);
        }
    }


}
