﻿namespace Project_1_TDD
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.DistancetextBox = new System.Windows.Forms.TextBox();
            this.logobox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PassengertextBox = new System.Windows.Forms.TextBox();
            this.Calcbutton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LYtextBox = new System.Windows.Forms.TextBox();
            this.OxygenTextBox = new System.Windows.Forms.TextBox();
            this.waterTextBox = new System.Windows.Forms.TextBox();
            this.Supportbutton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.logobox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // DistancetextBox
            // 
            this.DistancetextBox.Location = new System.Drawing.Point(6, 35);
            this.DistancetextBox.Name = "DistancetextBox";
            this.DistancetextBox.Size = new System.Drawing.Size(100, 20);
            this.DistancetextBox.TabIndex = 0;
            // 
            // logobox
            // 
            this.logobox.BackColor = System.Drawing.Color.Transparent;
            this.logobox.Image = ((System.Drawing.Image)(resources.GetObject("logobox.Image")));
            this.logobox.InitialImage = ((System.Drawing.Image)(resources.GetObject("logobox.InitialImage")));
            this.logobox.Location = new System.Drawing.Point(12, 12);
            this.logobox.Name = "logobox";
            this.logobox.Size = new System.Drawing.Size(104, 73);
            this.logobox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logobox.TabIndex = 1;
            this.logobox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Distance in Miles";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Passengers";
            // 
            // PassengertextBox
            // 
            this.PassengertextBox.Location = new System.Drawing.Point(6, 79);
            this.PassengertextBox.Name = "PassengertextBox";
            this.PassengertextBox.Size = new System.Drawing.Size(100, 20);
            this.PassengertextBox.TabIndex = 4;
            // 
            // Calcbutton
            // 
            this.Calcbutton.Location = new System.Drawing.Point(231, 235);
            this.Calcbutton.Name = "Calcbutton";
            this.Calcbutton.Size = new System.Drawing.Size(124, 23);
            this.Calcbutton.TabIndex = 5;
            this.Calcbutton.Text = "Calculate Time";
            this.Calcbutton.UseVisualStyleBackColor = true;
            this.Calcbutton.Click += new System.EventHandler(this.Calcbutton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "LY";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Oxygen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Water";
            // 
            // LYtextBox
            // 
            this.LYtextBox.Location = new System.Drawing.Point(58, 21);
            this.LYtextBox.Name = "LYtextBox";
            this.LYtextBox.Size = new System.Drawing.Size(100, 20);
            this.LYtextBox.TabIndex = 9;
            // 
            // OxygenTextBox
            // 
            this.OxygenTextBox.Location = new System.Drawing.Point(58, 62);
            this.OxygenTextBox.Name = "OxygenTextBox";
            this.OxygenTextBox.Size = new System.Drawing.Size(100, 20);
            this.OxygenTextBox.TabIndex = 10;
            // 
            // waterTextBox
            // 
            this.waterTextBox.Location = new System.Drawing.Point(58, 96);
            this.waterTextBox.Name = "waterTextBox";
            this.waterTextBox.Size = new System.Drawing.Size(100, 20);
            this.waterTextBox.TabIndex = 11;
            // 
            // Supportbutton
            // 
            this.Supportbutton.Location = new System.Drawing.Point(231, 321);
            this.Supportbutton.Name = "Supportbutton";
            this.Supportbutton.Size = new System.Drawing.Size(124, 23);
            this.Supportbutton.TabIndex = 12;
            this.Supportbutton.Text = "Calculate Support";
            this.Supportbutton.UseVisualStyleBackColor = true;
            this.Supportbutton.Click += new System.EventHandler(this.Supportbutton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DistancetextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.PassengertextBox);
            this.groupBox1.Location = new System.Drawing.Point(38, 234);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(120, 109);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inputs";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox2.Controls.Add(this.waterTextBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.OxygenTextBox);
            this.groupBox2.Controls.Add(this.LYtextBox);
            this.groupBox2.Location = new System.Drawing.Point(413, 222);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(164, 121);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Outputs";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(93, 123);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(382, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(122, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(414, 29);
            this.label6.TabIndex = 16;
            this.label6.Text = "Life Support Estimation Software";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(589, 356);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Supportbutton);
            this.Controls.Add(this.Calcbutton);
            this.Controls.Add(this.logobox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.logobox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox DistancetextBox;
        private System.Windows.Forms.PictureBox logobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PassengertextBox;
        private System.Windows.Forms.Button Calcbutton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox LYtextBox;
        private System.Windows.Forms.TextBox OxygenTextBox;
        private System.Windows.Forms.TextBox waterTextBox;
        private System.Windows.Forms.Button Supportbutton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
    }
}

