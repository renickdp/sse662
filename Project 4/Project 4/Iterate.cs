﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_4
{
    public class CustomIterator<T>
    {
        private IList<T> iterable;
        private int index;

        public CustomIterator(IList<T> iterable)
            {
            this.iterable = iterable;
            this.index = 0;            
            }

        public T next()
        {
            T item = this.iterable[index];
            this.index ++;
            return item;
        }
        
        public bool hasnext()
        {
            return this.index < this.iterable.Count();
        }


    }
}
