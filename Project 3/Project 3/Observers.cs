﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{

    public abstract class Event { };

    public abstract class Observable
    {
        public List<Observer> observers = new List<Observer>();
        public void register(Observer observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
        }
        public void unregister(Observer observer)
        {
            observers.Remove(observer);
        }
        public void notify(Event evt)
        {
            foreach (Observer item in observers)
            {
                item.update(evt);
            }
        }
    }

    public interface Observer
    {
        void update(Event evt);

    }

    public class CheckoutObservable : Observable { }


    public class CheckoutObserver : Observer
    {
        public void update(Event evt)
        {
            CheckoutEvent checkoutEvent = (CheckoutEvent)evt;
            IStorePage form = checkoutEvent.form;
            AbstractReceipt frm = form.getWindowFactory().CreateReceiptForm(checkoutEvent.order.receiptLines);

            ReceiptEvent receiptEvent = new ReceiptEvent(frm);
            form.getReceiptObservable().notify(receiptEvent);
            frm.Show();
        }
    }

    public class ReceiptObservable : Observable { }

    public class ReceiptObserver: Observer
    {
        public void update(Event evt)
        {
            ReceiptEvent receiptEvent = (ReceiptEvent)evt;
            AbstractReceipt receipt = receiptEvent.receipt;
            List<string> receiptLines = receipt.lines;
            receiptLines.Add("Thanks for visiting patterns food chain");
            receipt.lines = receiptLines;
        }
    }

    public class CandyReceiptObserver : Observer
    {
        public void update(Event evt)
        {
            ReceiptEvent receiptEvent = (ReceiptEvent)evt;
            AbstractReceipt receipt = receiptEvent.receipt;
            List<string> receiptLines = receipt.lines;
            receiptLines.Add("Thanks for shopping at the Candy Store!");
            receipt.lines = receiptLines;
        }
    }

    public class CheckoutEvent: Event
    {
        public Order order;
        public IStorePage form;

        public CheckoutEvent(Order order, IStorePage form)
        {
            this.order = order;
            this.form = form;
        }

    }
            
    public class ReceiptEvent : Event
    {
        public AbstractReceipt receipt;

        public ReceiptEvent(AbstractReceipt receipt)
        {
            this.receipt = receipt;
        }
    }
}
