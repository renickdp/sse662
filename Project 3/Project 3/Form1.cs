﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{
    public interface IStorePage
    {
        WindowFactory getWindowFactory();
        CheckoutObservable getCheckoutObservable();
        CheckoutObserver getCheckoutObserver();
        ReceiptObservable getReceiptObservable();
        Observer getReceiptObserver();
    }
        
    public partial class Form1 : Form, IStorePage

    {
        public WindowFactory windowFactory;
        public CheckoutObservable checkcoutobservable;
        public CheckoutObserver checkoutobserver;
        public Observer receiptObserver;
        public ReceiptObservable receiptObservable;
        public Dictionary<string, decimal> productPrices;

        public Form1(WindowFactory windowFactory)
        {
            InitializeComponent();
            this.productPrices = new Dictionary<string, decimal>()
            {
                ["Burger"] = 5.9m,
                ["Taco"] = 1.5m
            };

            this.checkoutobserver = new CheckoutObserver();
            this.checkcoutobservable = new CheckoutObservable();

            this.checkcoutobservable.register(checkoutobserver);

            this.receiptObservable = new ReceiptObservable();
            this.receiptObserver = new ReceiptObserver();

            this.receiptObservable.register(this.receiptObserver);

            this.windowFactory = windowFactory;
        }
        public WindowFactory getWindowFactory()
        {
            return this.windowFactory;
        }

        public CheckoutObservable getCheckoutObservable()
        {
            return this.checkcoutobservable;
        }

        public CheckoutObserver getCheckoutObserver()
        {
            return this.checkoutobserver;
        }

        public ReceiptObservable getReceiptObservable()
        {
            return this.receiptObservable;
        }

        public Observer getReceiptObserver()
        {
            return this.receiptObserver;
        }
        Order order = new Order();

        private void burgerbtn_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Product product = new Product(button.Text, this.productPrices[button.Text]);
            // order.addItem(new Product("Burger", 5.9m));
            order.addItem(product);

            this.AddToList(product);
        }


        public void AddToList(Product newProduct)
        {
            listBox1.Items.Add(newProduct);
            listBox1.Refresh();

            SubtotalTxtbox.Text = Convert.ToString(order.calculateTotal());
            TaxTextBox.Text = Convert.ToString(order.TaxCalculator.Calculate(order));

            Totaltextbox.Text = Convert.ToString(order.calculateTotal() + order.TaxCalculator.Calculate(order));
        }

        public void RemoveFromList(Product newProduct)
        {
            listBox1.Items.Remove(newProduct);
            listBox1.Refresh();

            SubtotalTxtbox.Text = Convert.ToString(order.calculateTotal());
            TaxTextBox.Text = Convert.ToString(order.TaxCalculator.Calculate(order));

            Totaltextbox.Text = Convert.ToString(order.calculateTotal() + order.TaxCalculator.Calculate(order));
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrencycomboBox.Text == "EUR")
            {
                order.SubtotalCalculator = new EurSubtotalCalculator();
            }
            else
            {
                order.SubtotalCalculator = new USDSubtotalCalculator();

            }
        }


        private void CouponTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Coupon coupon;
                if (CouponTextBox.Text == "free")
                {
                    coupon = new ItemCoupon("Cookie", order);
                }
                else if (CouponTextBox.Text == "10off")
                {
                    coupon = new PercentageOffCoupon(10, order);
                }
                else
                {
                    return;
                }
                if (this.order.appliedCoupon != null)
                {
                    this.order.appliedCoupon.undo();
                }

                coupon.execute();
                listBox1.Items.Clear();
                foreach (Product item in order.nameslist)
                {
                    AddToList(item);
                }
                this.order.appliedCoupon = coupon;
                listBox1.Refresh();
            }
        }

        private void CheckoutBtn_Click(object sender, EventArgs e)
        {
            CheckoutEvent evt = new CheckoutEvent(this.order, this);
            this.checkcoutobservable.notify(evt);

        }
    }
}

