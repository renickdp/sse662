﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScreenToGif;
using ScreenToGif.Util;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using ScreenToGif.Encoding;
using ScreenToGif.Capture;
using Moq;
using ScreenToGif.Pages;
using System.IO;

namespace GifRecorderTests
{
    [TestClass]
    public class TestLegacy
    {
        [TestMethod]
        public void TestLegacyWithArguments()
        {
            ArgumentUtil.FileName = @"..\..\images\blue.png";
            Legacy legacy = new Legacy();

            PrivateObject obj = new PrivateObject(legacy);
            Size lastsize = (Size)obj.GetField("_lastSize");

            Assert.AreEqual(300, lastsize.Height);
            Assert.AreEqual(500, lastsize.Width);
        }
    }

    [TestClass]
    public class TestImageUtil
    {
        [TestMethod]
        public void TestPaintTransparentAndCutNoChanges()
        {
            List<string> filenames = new List<string>();
            filenames.Add(@"..\..\images\blue.png");
            File.Copy(@"..\..\images\blue.png", @"..\..\images\blue_copy.png", true);
            filenames.Add(@"..\..\images\blue_copy.png");
            
            Color targetColor = Color.FromArgb(255, 0, 0);
            
            List<FrameInfo> changes = ImageUtil.PaintTransparentAndCut(filenames, targetColor, null);
            Assert.AreEqual(changes.Count, 2);

            FrameInfo first = changes[0];
            Assert.AreEqual(first.Image, @"..\..\images\blue.png");

            PixelUtil img1 = new PixelUtil(first.Image.From());

            Assert.AreEqual(img1.Width, 0);
            Assert.AreEqual(img1.Height, 0);

            FrameInfo second = changes[1];
            Assert.AreEqual(second.Image, @"..\..\images\blue_copy.png");
            PixelUtil img2 = new PixelUtil(second.Image.From());

            Assert.AreEqual(img2.Width, 0);
            Assert.AreEqual(img2.Height, 0);

        }
    }
}
