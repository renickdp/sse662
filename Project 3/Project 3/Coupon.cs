﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{

    public interface Coupon
    {
        void execute();
        void undo();
    };

    public class PercentageOffCoupon : Coupon
    {
        public int itemPercentage;
        public Order order;

        public PercentageOffCoupon(int itempercentage, Order order)
        {
            this.itemPercentage = itempercentage;
            this.order = order;
        }


        public void execute()
        {
            decimal percentageMultiplier = 1.0m - (this.itemPercentage / 100.0m);
            foreach (Product item in order.nameslist)
            {

                item.ProductPrice = item.ProductPrice * percentageMultiplier;

            }
        }

        public void undo()
        {
            decimal percentageMultiplier = 1.0m - (this.itemPercentage / 100.0m);
            foreach (Product item in order.nameslist)
            {
                item.ProductPrice = item.ProductPrice  / percentageMultiplier;
            }
        }
    }
    public class ItemCoupon : Coupon
    {
        public string itemCode;
        public Order order;

        public ItemCoupon(string itemcode,  Order order)
        {
            this.itemCode = itemcode;
            this.order = order;
        }


        public void execute()
        {
            Product couponproduct = new Product(itemCode, 0.0m);

            order.addItem(couponproduct);
        }

        public void undo()
        {
            for (int i = order.nameslist.Count - 1; i >= 0; i--)
            {

                Product product = order.nameslist[i];

                if (itemCode == product.productName && product.ProductPrice == 0.0m)
                {
                    order.nameslist.RemoveAt(i);
                }

            }

        }
    }
}
