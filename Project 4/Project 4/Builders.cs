﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Project_4
{
    public class ReportBuilder
    {
        private Color lineColor;
        private Color backgroundColor;
        private int lineThickness;
        private ReportMode mode;
        private List<InventoryChange> changes;

        public ReportBuilder()
        {
            this.lineColor = Color.FromArgb(0, 0, 255);
            this.backgroundColor = Color.FromArgb(255, 255, 255);
            this.lineThickness = 1;
            this.mode = ReportMode.TotalInputOutput;
        }

        public void setLineColor(Color color)
        {
            this.lineColor = color;
        }

        public void setBackgroundColor(Color color)
        {
            this.backgroundColor = color;
        }

        public void setLineThickness(int thickness)
        {
            this.lineThickness = thickness;
        }

        public void setMode(ReportMode mode)
        {
            this.mode = mode;
        }

        public void addData(List<InventoryChange> changes)
        {
            this.changes = changes;
        }

        public Form build()
        {
            Dictionary<string, List<Point<DateTime, int>>> points = new Dictionary<string, List<Point<DateTime, int>>>();
            int totalQty = 0;
            int totalAdded = 0;
            int totalTaken = 0;
            foreach (InventoryChange change in this.changes)
            {
                DateTime x = change.CreateTime;
                int y;
                string label;
                totalQty += change.QuantityChange;

                if (this.mode == ReportMode.TotalInputOutput)
                {
                    if (change.QuantityChange < 0)
                    {
                        label = "Quantity Removed";
                        totalTaken += -change.QuantityChange;
                        y = totalTaken;
                    }
                    else {
                        label = "Quantity Added";
                        totalAdded += change.QuantityChange;
                        y = totalAdded;
                    }
                }
                else if (this.mode == ReportMode.TotalQuantity)
                {
                    y = totalQty;
                    label = "Total Quantity";
                }
                else
                {
                    y = change.Quantity;
                    label = change.Inventory.Item.ItemName + " Quantity";
                }
                if (!points.ContainsKey(label))
                {
                    points[label] = new List<Point<DateTime, int>>();
                }
                points[label].Add(new Point<DateTime, int>(x, y));
            }
            return new ReportForm(points, this.lineColor, this.backgroundColor, this.lineThickness, this.mode);
        }
    }


}
