﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    public interface ICalculator
    {
        decimal Calculate(Order order);
    }

    //Strategy: calculate USD sub total
    public class USDSubtotalCalculator : ICalculator
    {
        public decimal Calculate(Order order)

        {
            decimal total = 0.0m;

            for (int i = 0; i < order.nameslist.Count; i++)
            {
                total = total + order.nameslist[i].ProductPrice;
            }


            return total;
            
        }
    }

    //Strategy EUR total
    public class EurSubtotalCalculator : ICalculator
    {
        public decimal Calculate(Order order)
        {
            decimal total = 0.0m;

            for (int i = 0; i < order.nameslist.Count; i++)
            {
                total = total + order.nameslist[i].ProductPrice;
            }

            total = total * 1.16m;

            return total;
        }
    }

    public class TaxCalculator : ICalculator
    {
        public decimal Calculate(Order order)
        {

            return order.SubtotalCalculator.Calculate(order) *0.07m;
        }
    }
}
