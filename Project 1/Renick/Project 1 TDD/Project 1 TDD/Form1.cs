﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_1_TDD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Calcbutton_Click(object sender, EventArgs e)
        {
           LYtextBox.Text = Convert.ToString(DistanceCalc.LightYearDist(Convert.ToInt64(DistancetextBox.Text)));
        }

        private void Supportbutton_Click(object sender, EventArgs e)
        {
            waterTextBox.Text = Convert.ToString(lifesupport.water(Convert.ToInt32(PassengertextBox.Text)));
            OxygenTextBox.Text = Convert.ToString(lifesupport.oxygen(Convert.ToInt32(PassengertextBox.Text), 
                                                                     Convert.ToDouble(LYtextBox.Text)));
        }
    }
}
