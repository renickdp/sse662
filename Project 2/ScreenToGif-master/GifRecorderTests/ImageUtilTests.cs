﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScreenToGif.Encoding;

namespace GifRecorderTests
{
    [TestClass]
    public class ImageUtilTests
    {
        private const string IMG_FOLDER = @"..\..\images";

        [TestMethod]
        public void GetBitmapsFromFileTest1()
        {
            string filePath = Path.Combine(IMG_FOLDER, "blue.png");
            Bitmap original = filePath.From();
            Size size = new Size(0, 0);            

            //List<Bitmap> images = ImageUtil.GetBitmapsFromFile(filePath, 0, size);
            List<Bitmap> images = ImageLoader.GetBitmapsFromFile(filePath, 0, size);
            
            Assert.AreEqual(1, images.Count());
            Assert.AreEqual(original.GetPixel(0, 0).B, images[0].GetPixel(0, 0).B);
        }

        [TestMethod]
        public void GetBitmapsFromFileTest2()
        {
            string filePath = Path.Combine(IMG_FOLDER, "red.png");
            Bitmap original = filePath.From();
            Size size = new Size(0, 0);

            //List<Bitmap> images = ImageUtil.GetBitmapsFromFile(filePath, 0, size);
            List<Bitmap> images = ImageLoader.GetBitmapsFromFile(filePath, 0, size);

            Assert.AreEqual(1, images.Count());
            Assert.AreEqual(original.GetPixel(0, 0).B, images[0].GetPixel(0, 0).B);
        }
    }
}
