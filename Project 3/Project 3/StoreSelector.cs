﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_3
{
    public partial class StoreSelector : Form
    {
        public StoreSelector()
        {
            InitializeComponent();
        }

        private void FoodStorebtn_Click(object sender, EventArgs e)
        {
            WindowFactory windowFactory = new FoodWindowFactory();
            Form newWindow = windowFactory.CreateStoreForm();
            newWindow.Show();
            this.Hide();

        }

        private void CandyStorebtn_Click(object sender, EventArgs e)
        {
            WindowFactory windowFactory = new CandyWindowFactory();
            Form newWindow = windowFactory.CreateStoreForm();
            newWindow.Show();
            this.Hide();
        }
    }
}
