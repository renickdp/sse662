﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ScreenToGif.Encoding
{
    public static class ImageLoader
    {
        private const string PngFormat = ".png";
        private const string JpegFormat = ".jpeg";
        private const string JpegFormat2 = ".jpg";
        private const string BmpFormat = ".bmp";
        private const string GifFormat = ".gif";

        /// <summary>
        /// Get frame(s) as list of bitmap(s) from jpeg, png, bmp or gif image file
        /// </summary>
        /// <param name="fileName">image file name</param>
        /// <param name="size">Used for resizing bitmap(s) </param>
        /// <param name="count">The amount of frames in the collection.</param>
        /// <exception cref="ArgumentException">[fileName] type
        /// isn't supported</exception>
        /// <exception cref="FileNotFoundException">[fileName] don't exist</exception>
        /// <returns>System.Collections.Generic.List of bitmap(s)</returns>
        public static List<Bitmap> GetBitmapsFromFile(string fileName, int count, Size size)
        {
            var multipleImages = false;

            //Check the existance of the image.
            if (!File.Exists(fileName))
                throw new FileNotFoundException("Unable to locate " + fileName);

            var extension = Path.GetExtension(fileName).ToLower();

            switch (extension)
            {
                case GifFormat:
                    multipleImages = true;
                    break;
                case PngFormat:
                case JpegFormat:
                case JpegFormat2:
                case BmpFormat:
                    break;
                default:
                    throw new ArgumentException("The selected file name is not supported", "fileName");
            }

            //Get list of frame(s) from image file.
            var myBitmaps = new List<Bitmap>();

            if (!multipleImages)
            {
                //Normal image files: jpeg, png or bmp.
                using (Bitmap img = fileName.From())
                {
                    if (count != 0)
                    {
                        myBitmaps.Add(ImageUtil.ResizeBitmap
                        (
                            img,
                            img.Width,
                            img.Height
                            ));
                    }
                    else
                    {
                        myBitmaps.Add(new Bitmap(img));
                    }
                }
            }
            else
            {
                //Gif File
                var binaryGif = GetFrames(fileName);

                foreach (var item in binaryGif)
                {
                    var tmpBitmap = ConvertBytesToImage(item);

                    if (tmpBitmap != null)
                    {
                        if (count != 0)
                        {
                            myBitmaps.Add(ImageUtil.ResizeBitmap(tmpBitmap,
                                size.Width,
                                size.Height
                                ));
                        }
                        else
                        {
                            //If there is no bitmap in the list, the first will not be resized.
                            myBitmaps.Add(tmpBitmap);
                        }
                    }
                }
            }

            return myBitmaps;
        }

        /// <summary>
        /// Return frame(s) as list of binary from jpeg, png, bmp or gif image file
        /// </summary>
        /// <param name="fileName">image file name</param>
        /// <returns>System.Collections.Generic.List of byte</returns>
        private static IEnumerable<byte[]> GetFrames(string fileName)
        {
            var tmpFrames = new List<byte[]>();

            // Check the image format to determine what format
            // the image will be saved to the memory stream in
            var guidToImageFormatMap = new Dictionary<Guid, ImageFormat>()
            {
                {ImageFormat.Bmp.Guid,  ImageFormat.Bmp},
                {ImageFormat.Gif.Guid,  ImageFormat.Png},
                {ImageFormat.Icon.Guid, ImageFormat.Png},
                {ImageFormat.Jpeg.Guid, ImageFormat.Jpeg},
                {ImageFormat.Png.Guid,  ImageFormat.Png}
            };

            using (var gifImg = Image.FromFile(fileName, true))
            {
                ImageFormat imageFormat = null;
                var imageGuid = gifImg.RawFormat.Guid;

                foreach (var pair in guidToImageFormatMap)
                {
                    if (imageGuid == pair.Key)
                    {
                        imageFormat = pair.Value;
                        break;
                    }
                }

                if (imageFormat == null)
                    throw new NoNullAllowedException("Unable to determine image format");

                //Get the frame count
                var dimension = new FrameDimension(gifImg.FrameDimensionsList[0]);
                var frameCount = gifImg.GetFrameCount(dimension);

                //Step through each frame
                for (var i = 0; i < frameCount; i++)
                {
                    //Set the active frame of the image and then
                    gifImg.SelectActiveFrame(dimension, i);

                    //write the bytes to the tmpFrames array
                    using (var ms = new MemoryStream())
                    {
                        gifImg.Save(ms, imageFormat);
                        tmpFrames.Add(ms.ToArray());
                    }
                }

                return tmpFrames;
            }
        }

        /// <summary>
        /// Convert bytes to Bitamp
        /// </summary>
        /// <param name="imageBytes">Image in a byte type</param>
        /// <returns>System.Drawing.Bitmap</returns>
        private static Bitmap ConvertBytesToImage(byte[] imageBytes)
        {
            if (imageBytes == null || imageBytes.Length == 0)
                return null;

            //Read bytes into a MemoryStream
            using (var ms = new MemoryStream(imageBytes))
            {
                //Recreate the frame from the MemoryStream
                using (var bmp = new Bitmap(ms))
                {
                    return (Bitmap)bmp.Clone();
                }
            }
        }
    }
}
