﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_TDD
{
    public class DistanceCalc
    {
        public static double LightYearDist(long Miles)
        {
            
            //var MPH = 670616629;

            var lightyear = 1 / (5.88 * Math.Pow(10, 12));

            var lightyearsaway = Miles * lightyear;

            lightyearsaway = lightyearsaway * Math.Pow(10, 2);

            return lightyearsaway;
        }

    }
}
