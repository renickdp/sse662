﻿namespace Project_3
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.burgerbtn = new System.Windows.Forms.Button();
            this.Tacobtn = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Totaltextbox = new System.Windows.Forms.TextBox();
            this.TaxTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CheckoutBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SubtotalTxtbox = new System.Windows.Forms.TextBox();
            this.CurrencycomboBox = new System.Windows.Forms.ComboBox();
            this.CouponTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // burgerbtn
            // 
            this.burgerbtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("burgerbtn.BackgroundImage")));
            this.burgerbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.burgerbtn.Location = new System.Drawing.Point(12, 78);
            this.burgerbtn.Name = "burgerbtn";
            this.burgerbtn.Size = new System.Drawing.Size(75, 70);
            this.burgerbtn.TabIndex = 0;
            this.burgerbtn.Text = "Lollipop";
            this.burgerbtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.burgerbtn.UseVisualStyleBackColor = true;
            this.burgerbtn.Click += new System.EventHandler(this.burgerbtn_Click);
            // 
            // Tacobtn
            // 
            this.Tacobtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Tacobtn.BackgroundImage")));
            this.Tacobtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Tacobtn.Location = new System.Drawing.Point(113, 79);
            this.Tacobtn.Name = "Tacobtn";
            this.Tacobtn.Size = new System.Drawing.Size(75, 69);
            this.Tacobtn.TabIndex = 1;
            this.Tacobtn.Text = "Licorice";
            this.Tacobtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Tacobtn.UseVisualStyleBackColor = true;
            this.Tacobtn.Click += new System.EventHandler(this.burgerbtn_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(34, 204);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(154, 186);
            this.listBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total";
            // 
            // Totaltextbox
            // 
            this.Totaltextbox.Location = new System.Drawing.Point(54, 82);
            this.Totaltextbox.Name = "Totaltextbox";
            this.Totaltextbox.Size = new System.Drawing.Size(54, 20);
            this.Totaltextbox.TabIndex = 4;
            // 
            // TaxTextBox
            // 
            this.TaxTextBox.Location = new System.Drawing.Point(54, 56);
            this.TaxTextBox.Name = "TaxTextBox";
            this.TaxTextBox.Size = new System.Drawing.Size(54, 20);
            this.TaxTextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Tax";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ravie", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(364, 34);
            this.label3.TabIndex = 7;
            this.label3.Text = "Patterns Candy chain";
            // 
            // CheckoutBtn
            // 
            this.CheckoutBtn.Location = new System.Drawing.Point(261, 363);
            this.CheckoutBtn.Name = "CheckoutBtn";
            this.CheckoutBtn.Size = new System.Drawing.Size(75, 26);
            this.CheckoutBtn.TabIndex = 8;
            this.CheckoutBtn.Text = "Checkout";
            this.CheckoutBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.CheckoutBtn.UseVisualStyleBackColor = true;
            this.CheckoutBtn.Click += new System.EventHandler(this.CheckoutBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.GhostWhite;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SubtotalTxtbox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Totaltextbox);
            this.groupBox1.Controls.Add(this.TaxTextBox);
            this.groupBox1.Location = new System.Drawing.Point(215, 248);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 104);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Subtotal";
            // 
            // SubtotalTxtbox
            // 
            this.SubtotalTxtbox.Location = new System.Drawing.Point(54, 10);
            this.SubtotalTxtbox.Name = "SubtotalTxtbox";
            this.SubtotalTxtbox.Size = new System.Drawing.Size(54, 20);
            this.SubtotalTxtbox.TabIndex = 7;
            // 
            // CurrencycomboBox
            // 
            this.CurrencycomboBox.FormattingEnabled = true;
            this.CurrencycomboBox.Items.AddRange(new object[] {
            "USD",
            "EUR"});
            this.CurrencycomboBox.Location = new System.Drawing.Point(311, 50);
            this.CurrencycomboBox.Name = "CurrencycomboBox";
            this.CurrencycomboBox.Size = new System.Drawing.Size(59, 21);
            this.CurrencycomboBox.TabIndex = 16;
            this.CurrencycomboBox.Text = "USD";
            this.CurrencycomboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // CouponTextBox
            // 
            this.CouponTextBox.Location = new System.Drawing.Point(215, 222);
            this.CouponTextBox.Name = "CouponTextBox";
            this.CouponTextBox.Size = new System.Drawing.Size(100, 20);
            this.CouponTextBox.TabIndex = 17;
            this.CouponTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CouponTextBox_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Coupon code";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(383, 412);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CouponTextBox);
            this.Controls.Add(this.CurrencycomboBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CheckoutBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Tacobtn);
            this.Controls.Add(this.burgerbtn);
            this.Name = "Form2";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button burgerbtn;
        private System.Windows.Forms.Button Tacobtn;
        public System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Totaltextbox;
        private System.Windows.Forms.TextBox TaxTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CheckoutBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SubtotalTxtbox;
        private System.Windows.Forms.ComboBox CurrencycomboBox;
        private System.Windows.Forms.TextBox CouponTextBox;
        private System.Windows.Forms.Label label5;
    }
}

