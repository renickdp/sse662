using System.Linq;

namespace Project_4
{
    partial class Inventory
    {
        public object getState()
        {
            string[] vals = {this.ProductId.ToString(),
                             this.Quantity.ToString()};
            string state = string.Join(",", vals);
            return new Memento(state);
        } 

        public void setState(object state)
        {
            Memento mem = (Memento)state;
            string[] vals = mem.State.Split(',');
            int productId = int.Parse(vals[0]);
            int qty = int.Parse(vals[1]);
            this.Quantity = qty;
            this.ProductId = productId;
        }
    }

    partial class OrderedItem
    {
        public override string ToString()
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            Item item = db.Items.Single(i => i.Id == this.ItemId);
            return item.ItemName + "\t" + this.OrderedAmount.ToString() + "\t" + item.ItemPrice.ToString();
        }
    }

    partial class PurchaseOrderItem
    {
        public override string ToString()
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            Item item = db.Items.Single(i => i.Id == this.ItemId);
            return item.ItemName + "\t" + this.OrderedAmount.ToString() + "\t" + item.ItemPrice.ToString();
        }
    }
}